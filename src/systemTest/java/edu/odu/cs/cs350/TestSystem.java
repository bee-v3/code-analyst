package edu.odu.cs.cs350;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class TestSystem {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	/**
	 * Tests the system using the help argument. Checks System.out for the usage
	 * instructions.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testSystemOutputHelp() throws IOException {
		String[] testHelp = { "-help" };

		CodeComp.main(testHelp);
		assertThat(outContent.toString().contains("Available options:"), is(true));
		assertThat(outContent.toString().contains("Insufficient Parameters. For Help: java -jar CodeComp.jar -help"),
				is(false));
		assertThat(outContent.toString().contains("ajones"), is(false));
		assertThat(outContent.toString().contains("jsmith"), is(false));
		assertThat(outContent.toString().contains("tlee"), is(false));
		assertThat(outContent.toString().contains("LOC:"), is(false));
	}

	/**
	 * Tests the system using incorrect directories. Checks System.out for the
	 * insufficient parameters error message.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testSystemOutputError() throws IOException {
		String[] testError1 = { "-t", "-help" };

		CodeComp.main(testError1);
		assertThat(outContent.toString().contains("Insufficient Parameters. For Help: java -jar CodeComp.jar -help"),
				is(true));
		assertThat(outContent.toString().contains("ajones"), is(false));
		assertThat(outContent.toString().contains("jsmith"), is(false));
		assertThat(outContent.toString().contains("tlee"), is(false));
		assertThat(outContent.toString().contains("LOC:"), is(false));
	}

	/**
	 * Tests the system using correct directories. Checks System.out for output of
	 * student names
	 * 
	 * @throws IOException
	 */
	@Test
	public void testSystemOutputCorrect() throws IOException {
		String[] testCorrect = { "./src/systemTest/data", "./src/systemTest/data/" };
		CodeComp.main(testCorrect);
		assertThat(outContent.toString().contains("ajones"), is(true));
		assertThat(outContent.toString().contains("jsmith"), is(true));
		assertThat(outContent.toString().contains("tlee"), is(true));
		assertThat(outContent.toString().contains("LOC:"), is(true));

		File testFile = new File("./src/systemTest/data/Report.xls");
		assertThat(testFile.exists(), is(true));
	}

	@After
	public void restoreStreams() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}
}
