package edu.odu.cs.cs350;

import static org.junit.Assert.*;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;




public class TestToken {


  String lexeme = "a test sequence";
  boolean defaultJavaOrCPP = true;
  Token emptyToken = new Token();
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for Token default constructor
   */
  @Test
  public final void testTokenDefaultConstructor() {
	  assertThat(emptyToken.getType(), is(TokenEnum.EOF));
	  assertThat(emptyToken.getIsJava(), is(false));
	  assertThat(emptyToken.getLexeme(), is(""));
	  assertThat(emptyToken.toString().contains(emptyToken.getType().toString()), is(true));
  }
  /**
   * Test method for two value Token constructor.
   */
  @Test
  public final void testTokenConstructorTwo() {
    Token s1 = new Token(TokenEnum.EOF, defaultJavaOrCPP);
    
    assertThat(s1.getIsJava(), is(true));
    assertThat(s1.getIsJava(), is(not(false)));
    assertThat(s1.getIsJava(), is(not(emptyToken.getIsJava())));    
    s1.setisJava(false);
    assertThat(s1.getIsJava(), is(false));
    assertThat(s1.getIsJava(), is(not(true)));
    assertThat(s1.getIsJava(), is(emptyToken.getIsJava()));    
  }

  /**
   * Test method for three value Token constructor.
   */
  @Test
  public final void testTokenConstructorThree() {
    Token s2 = new Token(TokenEnum.EOF, lexeme, defaultJavaOrCPP);
    
    assertThat(s2.getType(), is(TokenEnum.EOF));
    assertThat(s2.getLexeme(), is(lexeme));
    assertThat(s2.getIsJava(), is(defaultJavaOrCPP));
    assertThat(s2.toString().contains("sequence"), is(true));
  }
  
  /**
   * Test method for reading tokens from a file.
   */
  @Test
  public final void testTokenReader() {

	  File testFile = new File("./src/test/data/studentsubmissions/tokentest", "test1.java");
	  try {
		  BufferedReader testReader = new BufferedReader(new FileReader(testFile));
		  List<Token> t1 = new ArrayList<Token>();
		  JavaScanner scanner = new JavaScanner(testReader);
		
		  try {	
			  Token token = scanner.yylex();				
			  while(token.getType() != TokenEnum.EOF ) {
				  t1.add( token );
				  token = scanner.yylex();
			  }
			  assertThat(t1.size(), is(11));
		}catch(IOException e) {}
		
		try {
			File file = new File("./src/test/data/studentsubmissions/tokentest", "results.txt");		
			FileWriter writer = new FileWriter(file.getAbsoluteFile());
	
			try {
				for(Token s: t1) {
					writer.write(s.getType().toString() + " : " + s.getLexeme() + " Line:" +
								s.getLine() + " Column: " + s.getColumn());
					writer.write(System.getProperty("line.separator"));
				}					
				writer.close();			
			} catch (IOException e) {}
		}catch (IOException e) {}
	  }catch (FileNotFoundException ex) {}	    
  }
  
  /**
   * Test method for Token clone function.
   */
  @Test
  public final void testClone() {
    Token s1 = new Token(TokenEnum.EOF, 29, 30, lexeme, defaultJavaOrCPP);
    Token s2 = (Token)s1.clone();
    assertEquals(s1.getLexeme(), s2.getLexeme());
    assertEquals(s1.getType(), s2.getType());
    assertEquals(s1.getLine(), s2.getLine());
    assertEquals(s1.getColumn(), s2.getColumn());
    assertEquals(s1.getIsJava(), s1.getIsJava());
    assertEquals(s1.hashCode(), s1.hashCode());
    assertFalse(s2.equals(emptyToken));
  }
