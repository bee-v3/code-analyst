package edu.odu.cs.cs350;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

/**
 * Tests the Instructor class
 */
public class TestInstructor
{	
	String[] emptyArguments = null;
	Instructor emptyInstructor = new Instructor();	
	String DEFAULT_SHEET_NAME = "Report";
	
	/**
	 * Tests the default constructor.
	 */
	@Test
	public void testEmptyInstructor()
	{
		assertThat(emptyInstructor.getIsError(), is(true));
		assertThat(emptyInstructor.getIsHelp(), is(false));
		assertThat(emptyInstructor.getCustomTemplate(), is(false));
		assertThat(emptyInstructor.getTemplateName(), equalTo(""));
		assertThat(emptyInstructor.getAssignmentDirectory(), equalTo(""));		
		assertThat(emptyInstructor.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(emptyInstructor.getOutputDirectory(), equalTo(""));		
	}
	
	/**
	 * Tests the getters and setters of Instructor to set private member variables
	 */
	@Test
	public void testSetters()
	{
		Instructor setterInstructor = new Instructor();
		assertThat(setterInstructor.getIsError(), is(true));
		assertThat(setterInstructor.getIsHelp(), is(false));
		assertThat(setterInstructor.getCustomTemplate(), is(false));
		assertThat(setterInstructor.getTemplateName(), equalTo(""));
		assertThat(setterInstructor.getAssignmentDirectory(), equalTo(""));		
		assertThat(setterInstructor.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(setterInstructor.getOutputDirectory(), equalTo(""));	
		
		setterInstructor.setIsError(false);
		setterInstructor.setIsHelp(true);
		setterInstructor.setCustomTemplate(true);
		setterInstructor.setTemplateName("Custom_spreadsheet_template");
		setterInstructor.setReportSheetName("Custom_sheet_name");
		setterInstructor.setOutputDirectory("./test/output");
		setterInstructor.setAssignmentDirectory("./test/assignment");
		assertThat(setterInstructor.getIsError(), is(false));
		assertThat(setterInstructor.getIsHelp(), is(true));
		assertThat(setterInstructor.getCustomTemplate(), is(true));
		assertThat(setterInstructor.getTemplateName(), equalTo("Custom_spreadsheet_template"));
		assertThat(setterInstructor.getAssignmentDirectory(), equalTo("./test/assignment"));		
		assertThat(setterInstructor.getReportSheetName(), equalTo("Custom_sheet_name"));
		assertThat(setterInstructor.getOutputDirectory(), equalTo("./test/output"));
	}
	
	/**
	 * Test initializing Instructor with correct arguments
	 */
	@Test
	public void testCorrectArgs()
	{
		// Two arrangements of six correct arguments
		String[] sixArgs1 = {"-template", "spreadsheet.xls", "-report", "StudentScores", "./test/assignment", "./test/output"};
		String[] sixArgs2 = {"-report", "StudentScores2", "-template", "spreadsheet2.xls", "./test/assignment", "./test/output"};				
		
		Instructor argInstructor61 = new Instructor(sixArgs1);
		Instructor argInstructor62 = new Instructor(sixArgs2);
		
		// Test argInstructor1
		assertThat(argInstructor61.getIsError(), is(false));
		assertThat(argInstructor61.getIsHelp(), is(false));
		assertThat(argInstructor61.getCustomTemplate(), is(true));
		assertThat(argInstructor61.getTemplateName(), equalTo("spreadsheet.xls"));
		assertThat(argInstructor61.getReportSheetName(), equalTo("StudentScores"));
		assertThat(argInstructor61.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(argInstructor61.getOutputDirectory(), equalTo("./test/output"));
		
		// Test argInstructor2
		assertThat(argInstructor62.getIsError(), equalTo(argInstructor61.getIsError()));
		assertThat(argInstructor62.getIsHelp(), equalTo(argInstructor61.getIsHelp()));
		assertThat(argInstructor62.getCustomTemplate(), equalTo(argInstructor61.getCustomTemplate()));
		assertThat(argInstructor62.getTemplateName(), equalTo("spreadsheet2.xls"));
		assertThat(argInstructor62.getReportSheetName(), equalTo("StudentScores2"));
		assertThat(argInstructor62.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(argInstructor62.getOutputDirectory(), equalTo("./test/output"));
		
		// Two arrangement of four correct arguments
		String[] fourArgs1 = {"-report", "StudentScores4a", "./test/assignment4", "./test/output4"};
		String[] fourArgs2 = {"-template", "spreadsheet4a.xls", "./test/assignment4", "./test/output4"};
		
		Instructor argInstructor41 = new Instructor(fourArgs1);
		Instructor argInstructor42 = new Instructor(fourArgs2);
		// Test argInstructor41
		assertThat(argInstructor41.getIsError(), is(false));
		assertThat(argInstructor41.getIsHelp(), is(false));
		assertThat(argInstructor41.getCustomTemplate(), is(false));
		assertThat(argInstructor41.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(argInstructor41.getReportSheetName(), equalTo("StudentScores4a"));
		assertThat(argInstructor41.getAssignmentDirectory(), equalTo("./test/assignment4"));
		assertThat(argInstructor41.getOutputDirectory(), equalTo("./test/output4"));
		
		// Test argInstructor42
		assertThat(argInstructor42.getIsError(), is(false));
		assertThat(argInstructor42.getIsHelp(), is(false));
		assertThat(argInstructor42.getCustomTemplate(), is(true));
		assertThat(argInstructor42.getTemplateName(), equalTo("spreadsheet4a.xls"));
		assertThat(argInstructor42.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(argInstructor42.getAssignmentDirectory(), equalTo(argInstructor41.getAssignmentDirectory()));
		assertThat(argInstructor42.getOutputDirectory(), equalTo(argInstructor41.getOutputDirectory()));
		
		// Two correct arguments
		String[] twoArgs = {"./test/twoargs", "./test/twoout"};
		
		Instructor argInstructor21 = new Instructor(twoArgs);
		
		assertThat(argInstructor21.getIsError(), is(false));
		assertThat(argInstructor21.getIsHelp(), is(false));
		assertThat(argInstructor21.getCustomTemplate(), is(false));
		assertThat(argInstructor21.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(argInstructor21.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(argInstructor21.getAssignmentDirectory(), equalTo("./test/twoargs"));
		assertThat(argInstructor21.getOutputDirectory(), equalTo("./test/twoout"));
		
		// The only possible correct single argument
		String[] helpArg = {"-help"};
		
		Instructor helpInstructor = new Instructor(helpArg);
		assertThat(helpInstructor.getIsError(), is(false));
		assertThat(helpInstructor.getIsHelp(), is(true));
		assertThat(helpInstructor.getCustomTemplate(), is(false));
		assertThat(helpInstructor.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(helpInstructor.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(helpInstructor.getAssignmentDirectory(), equalTo(""));
		assertThat(helpInstructor.getOutputDirectory(), equalTo(""));
	}
	
	/**
	 * Test initializing Instructor with incorrect arguments
	 */
	@Test
	public void testIncorrectArgs()
	{
		// Two arrangements of 6 arguments with incorrect parameters.
		String[] sixArgs1 = {"-report", "spreadsheet", "-r", "score", "./test/assignment", "./test/output"};
		String[] sixArgs2 = {"-help", "spreadsheet", "-report", "sheet", "./test/assignment", "./test/output"};	
		
		Instructor wrongInstructor61 = new Instructor(sixArgs1);
		
		assertThat(wrongInstructor61.getIsError(), is(true));
		assertThat(wrongInstructor61.getIsHelp(), is(false));
		assertThat(wrongInstructor61.getCustomTemplate(), is(false));
		assertThat(wrongInstructor61.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor61.getReportSheetName(), equalTo("spreadsheet"));
		assertThat(wrongInstructor61.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(wrongInstructor61.getOutputDirectory(), equalTo("./test/output"));
		
		Instructor wrongInstructor62 = new Instructor(sixArgs2);
		
		assertThat(wrongInstructor62.getIsError(), is(true));
		assertThat(wrongInstructor62.getIsHelp(), is(false));
		assertThat(wrongInstructor62.getCustomTemplate(), is(false));
		assertThat(wrongInstructor62.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor62.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor62.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(wrongInstructor62.getOutputDirectory(), equalTo("./test/output"));
		
		String[] fiveArgs1 = {"-template", "-report", "-help", "./test/assignment", "./test/output"};		
		
		Instructor wrongInstructor51 = new Instructor(fiveArgs1);
		
		assertThat(wrongInstructor51.getIsError(), is(true));
		assertThat(wrongInstructor51.getIsHelp(), is(false));
		assertThat(wrongInstructor51.getCustomTemplate(), is(false));
		assertThat(wrongInstructor51.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor51.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor51.getAssignmentDirectory(), equalTo(emptyInstructor.getAssignmentDirectory()));
		assertThat(wrongInstructor51.getOutputDirectory(), equalTo(emptyInstructor.getOutputDirectory()));
		
		String[] threeArgs1 = {"-help", "-template", "./test/assignment", "./test/output"};
		String[] threeArgs2 = {"-wrong", "-help", "./test/assignment", "./test/output"};
		
		Instructor wrongInstructor31 = new Instructor(threeArgs1);
		
		assertThat(wrongInstructor31.getIsError(), is(true));
		assertThat(wrongInstructor31.getIsHelp(), is(false));
		assertThat(wrongInstructor31.getCustomTemplate(), is(false));
		assertThat(wrongInstructor31.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor31.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor31.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(wrongInstructor31.getOutputDirectory(), equalTo("./test/output"));
		
		Instructor wrongInstructor32 = new Instructor(threeArgs2);
		
		assertThat(wrongInstructor32.getIsError(), is(true));
		assertThat(wrongInstructor32.getIsHelp(), is(false));
		assertThat(wrongInstructor32.getCustomTemplate(), is(false));
		assertThat(wrongInstructor32.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor32.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor32.getAssignmentDirectory(), equalTo("./test/assignment"));
		assertThat(wrongInstructor32.getOutputDirectory(), equalTo("./test/output"));
		
		String[] oneArgs1 = {"test"};
		String[] oneArgs2 = {"-template"};
		String[] oneArgs3 = {""};
		
		Instructor wrongInstructor11 = new Instructor(oneArgs1);
		
		assertThat(wrongInstructor11.getIsError(), is(true));
		assertThat(wrongInstructor11.getIsHelp(), is(false));
		assertThat(wrongInstructor11.getCustomTemplate(), is(false));
		assertThat(wrongInstructor11.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor11.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor11.getAssignmentDirectory(), equalTo(emptyInstructor.getAssignmentDirectory()));
		assertThat(wrongInstructor11.getOutputDirectory(), equalTo(emptyInstructor.getOutputDirectory()));
		
		Instructor wrongInstructor12 = new Instructor(oneArgs2);
		
		assertThat(wrongInstructor12.getIsError(), is(true));
		assertThat(wrongInstructor12.getIsHelp(), is(false));
		assertThat(wrongInstructor12.getCustomTemplate(), is(false));
		assertThat(wrongInstructor12.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor12.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor12.getAssignmentDirectory(), equalTo(emptyInstructor.getAssignmentDirectory()));
		assertThat(wrongInstructor12.getOutputDirectory(), equalTo(emptyInstructor.getOutputDirectory()));
		
		Instructor wrongInstructor13 = new Instructor(oneArgs3);
		
		assertThat(wrongInstructor13.getIsError(), is(true));
		assertThat(wrongInstructor13.getIsHelp(), is(false));
		assertThat(wrongInstructor13.getCustomTemplate(), is(false));
		assertThat(wrongInstructor13.getTemplateName(), equalTo(emptyInstructor.getTemplateName()));
		assertThat(wrongInstructor13.getReportSheetName(), equalTo(DEFAULT_SHEET_NAME));
		assertThat(wrongInstructor13.getAssignmentDirectory(), equalTo(emptyInstructor.getAssignmentDirectory()));
		assertThat(wrongInstructor13.getOutputDirectory(), equalTo(emptyInstructor.getOutputDirectory()));
	}
		
	/**
	 * Tests getStudentDirectoryNames(String) which returns a list of directory names in the given assignment directory.
	 */
	@Test
	public void testAssignmentDirectory()
	{
		String[] testParams = {"./src/test/data/studentsubmissions", "."}; 
		
		Instructor testStudentFolders = new Instructor(testParams);
		
		List<String> studentfolders = testStudentFolders.getStudentDirectoryNames(testStudentFolders.getAssignmentDirectory());

		assertThat(studentfolders.isEmpty(), is(false));
		assertThat(studentfolders, not(hasItem("asmith.21")));
		assertThat(studentfolders, hasItem("asmith.99"));
		assertThat(studentfolders, hasItem("cjackson"));
		assertThat(studentfolders, not(hasItem("cjackson.Af99")));
		assertThat(studentfolders, hasItem("hlee"));
		assertThat(studentfolders, hasItem("jsmith"));
		assertThat(studentfolders, not(hasItem("jsmith.11")));
		assertThat(studentfolders, not(hasItem("jsmith.66")));
		assertThat(studentfolders, not(hasItem("jsmith.99")));
	}	
	
	/**
	 * Tests getStudents(List<String>), getStudents(String), addStudent(Student), and getStudentList().
	 */
	@Test
	public void testGetStudents()
	{
		String[] testParams = {"./src/test/data/studentsubmissions", "."}; 
		
		Instructor testStudents = new Instructor(testParams);
		List<String> stringdirectories = testStudents.getStudentDirectoryNames(testStudents.getAssignmentDirectory());
		assertThat(stringdirectories.isEmpty(), is(false));
		testStudents.getStudents(stringdirectories);
		List<Student> testStudentList = testStudents.getStudentList();
		List<String> testNames = new ArrayList<String>();
		
		for(Student s : testStudentList)
		{
			testNames.add(s.getName());
		}
		
		assertThat(testStudents.getIsError(), is(false));
		assertThat(testNames, not(hasItem("asmith.21")));
		assertThat(testNames, hasItem("asmith"));
		assertThat(testNames, hasItem("cjackson"));
		assertThat(testNames, not(hasItem("cjackson.Af99")));
		assertThat(testNames, hasItem("hlee"));
		assertThat(testNames, hasItem("jsmith"));
		assertThat(testNames, not(hasItem("jsmith.11")));
		assertThat(testNames, not(hasItem("jsmith.66")));
		assertThat(testNames, not(hasItem("jsmith.99")));
	}
	/**
	 *  Tests {@link Instructor#scoreStudents()}
	 * @throws IOException 
	 * 
	 */
	@Test
	public void testScoreStudents() throws IOException {
		List<String> ts;
		List<String> ts2;
		List<Double> ts3;
		List<Double> ts4;
		Instructor testInst = new Instructor();
		Student s1 = new Student("zdevo.01");
		Student s2 = new Student("zdevo.02");
		Student s3 = new Student("zdevo.03");
		Student s4 = new Student("zdevo.04");
		File file = new File("./src/test/data/studentsubmissions/scoreTest", "scoretest1.cpp");
		File file2 = new File("./src/test/data/studentsubmissions/scoreTest", "scoretest2.cpp");
		File file3 = new File("./src/test/data/studentsubmissions/scoreTest", "scoretest3.cpp");
	    file.getParentFile().mkdirs();
	    FileWriter writer = new FileWriter(file.getAbsoluteFile());
	    file.deleteOnExit();
		writer.write("main{\n");
		writer.write("int a = 1;\n");
		writer.write("int b = 2;\n");
		writer.write("int c = 3;\n");
		writer.write("int c = a + b;\n");
		writer.write("}");
		writer.close();
		FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
	    file2.deleteOnExit();
		writer2.write("main{\n");
		writer2.write("char c = a;\n");
		writer2.write("cout << a\n");
		writer2.write("}");
		writer2.close();
		FileWriter writer3 = new FileWriter(file3.getAbsoluteFile());
	    file3.deleteOnExit();
		writer3.write("%");
		writer3.close();
		
		
		
	    Submission su1 = new Submission("scoretest1.cpp",file);
	    Submission su2 = new Submission("scoretest2.cpp",file2);
	    Submission su3 = new Submission("scoretest3.cpp",file3);
	    
	    s1.addSubmission(su1);
	    s1.addSubmission(su2);
	    s2.addSubmission(su1);
	    s2.addSubmission(su2);
	    s3.addSubmission(su1);
	    s4.addSubmission(su3);
	    
	    testInst.addStudent(s1);
	    testInst.addStudent(s2);
	    testInst.addStudent(s3);
	    testInst.addStudent(s4);
	    
	    testInst.scoreStudents();
	    ts = testInst.getScoredStudentOne();
	    ts2 = testInst.getScoredStudentTwo();
	    ts3 = testInst.getRawScores();
	    ts4 = testInst.getNormalizedScores();
	    
	    
	    assertEquals(6, ts.size());
	    assertEquals(6, ts2.size());
	    assertEquals(6, ts3.size());
	    assertEquals(ts.get(0), "zdevo.01");
	    assertEquals(ts.get(3), "zdevo.02");
	    assertEquals(ts.get(5), "zdevo.03");
	    assertEquals(ts2.get(0), "zdevo.02");
	    assertEquals(ts2.get(1), "zdevo.03");
	    assertEquals(ts2.get(2), "zdevo.04");
	    assertEquals(ts2.get(3), "zdevo.03");
	    assertEquals(ts2.get(4), "zdevo.04");
	    assertEquals(ts2.get(5), "zdevo.04");
	    assertTrue(ts3.get(0) > ts3.get(1));
	    assertTrue(ts3.get(1) > ts3.get(2));
	    assertTrue(ts4.get(0) > ts4.get(1));
	    assertTrue(ts4.get(1) > ts4.get(2)); 
	}
	
	/**
	 *  Tests {@link Instructor#instructorDriver()}
	 * @throws IOException 
	 * 
	 */
	@Test
	public void testInstructorDriver() throws IOException {
		List<String> ts;
		List<String> ts2;
		List<Double> ts3;
		List<Double> ts4;
		Instructor testInstDriv = new Instructor();
		File file = new File("./src/test/data/studentsubmissions/DriverTest/student1/", "scoretest1.cpp");
		File file2 = new File("./src/test/data/studentsubmissions/DriverTest/student1/", "scoretest2.cpp");
		File file3 = new File("./src/test/data/studentsubmissions/DriverTest/student2/", "scoretest1.cpp");
		File file4 = new File("./src/test/data/studentsubmissions/DriverTest/student2/", "scoretest2.cpp");
		File file5 = new File("./src/test/data/studentsubmissions/DriverTest/student3/", "scoretest1.cpp");
		File file6 = new File("./src/test/data/studentsubmissions/DriverTest/student4/", "scoretest3.cpp");
	    file.getParentFile().mkdirs();
	    file3.getParentFile().mkdirs();
	    file5.getParentFile().mkdirs();
	    file6.getParentFile().mkdirs();
	    
	    FileWriter writer = new FileWriter(file.getAbsoluteFile());
	    file.deleteOnExit();
		writer.write("main{\n");
		writer.write("int a = 1;\n");
		writer.write("int b = 2;\n");
		writer.write("int c = 3;\n");
		writer.write("int c = a + b;\n");
		writer.write("}");
		writer.close();
		FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
	    file2.deleteOnExit();
		writer2.write("main{\n");
		writer2.write("char c = a;\n");
		writer2.write("cout << a\n");
		writer2.write("}");
		writer2.close();
		FileWriter writer3 = new FileWriter(file3.getAbsoluteFile());
	    file3.deleteOnExit();
		writer3.write("main{\n");
		writer3.write("int a = 1;\n");
		writer3.write("int b = 2;\n");
		writer3.write("int c = 3;\n");
		writer3.write("int c = a + b;\n");
		writer3.write("}");
		writer3.close();
		FileWriter writer4 = new FileWriter(file4.getAbsoluteFile());
	    file4.deleteOnExit();
		writer4.write("main{\n");
		writer4.write("char c = a;\n");
		writer4.write("cout << a\n");
		writer4.write("}");
		writer4.close();
		FileWriter writer5 = new FileWriter(file5.getAbsoluteFile());
	    file.deleteOnExit();
		writer5.write("main{\n");
		writer5.write("int a = 1;\n");
		writer5.write("int b = 2;\n");
		writer5.write("int c = 3;\n");
		writer5.write("int c = a + b;\n");
		writer5.write("}");
		writer5.close();
		FileWriter writer6 = new FileWriter(file6.getAbsoluteFile());
	    file6.deleteOnExit();
		writer6.write("%");
		writer6.close();
		
		List<String> sDirectories;
		testInstDriv.setAssignmentDirectory("./src/test/data/studentsubmissions/DriverTest/");
		sDirectories = testInstDriv.getStudentDirectoryNames("./src/test/data/studentsubmissions/DriverTest/");
		
		testInstDriv.getStudents(sDirectories);
		testInstDriv.setCustomTemplate(true);
		testInstDriv.setTemplateName("InstructorDriver"); 
		testInstDriv.scoreStudents();
	    ts = testInstDriv.getScoredStudentOne();
	    ts2 = testInstDriv.getScoredStudentTwo();
	    ts3 = testInstDriv.getRawScores();
	    ts4 = testInstDriv.getNormalizedScores();
	    Spreadsheet sheet1 = new Spreadsheet(ts, ts2, ts3, ts4);
	    sheet1.setSheetName(testInstDriv.getTemplateName());
	    sheet1.generateSheet();
	    
	    
	    assertEquals(6, ts.size());
	    assertEquals(6, ts2.size());
	    assertEquals(6, ts3.size());
	    assertEquals(6, ts4.size());

	}
	
	 /**
	   * Test method for {@link Instructor#setScoredStudentOne(List)}
	   */
	  @Test
	  public final void testSetScoredStudentOne(){
		  Instructor testInst = new Instructor();
		  Student s1 = new Student("test1");
		  Student s2 = new Student("test2");
		  List<String> templist = new ArrayList<String>();
		  templist.add(s1.getName());
		  templist.add(s2.getName());
		  testInst.setScoredStudentOne(templist);
		  assertEquals(2, testInst.getScoredStudentOne().size());
		  assertEquals(0, testInst.getScoredStudentTwo().size());
		  assertEquals(0, testInst.getRawScores().size());
		  assertEquals(0, testInst.getNormalizedScores().size());
		  assertEquals("test1", testInst.getScoredStudentOne().get(0));
		  assertEquals("test2", testInst.getScoredStudentOne().get(1));
		  assertNotEquals(testInst, emptyInstructor);
	  }
	  /**
	   * Test method for {@link Instructor#setScoredStudentTwo(List)}
	   */
	  @Test
	  public final void testSetScoredStudentTwo(){
		  Instructor testInst = new Instructor();
		  Student s1 = new Student("test1");
		  Student s2 = new Student("test2");
		  List<String> templist = new ArrayList<String>();
		  templist.add(s1.getName());
		  templist.add(s2.getName());
		  testInst.setScoredStudentTwo(templist);
		  assertEquals(0, testInst.getScoredStudentOne().size());
		  assertEquals(2, testInst.getScoredStudentTwo().size());
		  assertEquals(0, testInst.getRawScores().size());
		  assertEquals(0, testInst.getNormalizedScores().size());
		  assertEquals("test1", testInst.getScoredStudentTwo().get(0));
		  assertEquals("test2", testInst.getScoredStudentTwo().get(1));
		  assertNotEquals(testInst, emptyInstructor);
	  }
	  /**
	   * Test method for {@link Instructor#getRawScores()}
	   */
	  @Test
	  public final void testSetRawScores(){
		  Instructor testInst = new Instructor();
		  List<Double> templist = new ArrayList<Double>();
		  templist.add(0.0);
		  templist.add(100.0);
		  testInst.setRawScores(templist);
		  assertEquals(0, testInst.getScoredStudentOne().size());
		  assertEquals(0, testInst.getScoredStudentTwo().size());
		  assertEquals(2, testInst.getRawScores().size());
		  assertEquals(0, testInst.getNormalizedScores().size());
		  assertTrue(0.0 == testInst.getRawScores().get(0));
		  assertTrue(100.0 == testInst.getRawScores().get(1));
		  assertNotEquals(testInst, emptyInstructor);
	  }
	  /**
	   * Test method for {@link Instructor#getNormalizedScores()}
	   */
	  @Test
	  public final void testSetNormalizedScores(){
		  Instructor testInst = new Instructor();
		  List<Double> templist = new ArrayList<Double>();
		  templist.add(0.0);
		  templist.add(100.0);
		  testInst.setNormalizedScores(templist);
		  assertEquals(0, testInst.getScoredStudentOne().size());
		  assertEquals(0, testInst.getScoredStudentTwo().size());
		  assertEquals(0, testInst.getRawScores().size());
		  assertEquals(2, testInst.getNormalizedScores().size());
		  assertTrue(0.0 == testInst.getNormalizedScores().get(0));
		  assertTrue(100.0 == testInst.getNormalizedScores().get(1));
		  assertNotEquals(testInst, emptyInstructor);
	  }
}



