package edu.odu.cs.cs350;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestSubmission {


  String defaultName = "a name";
  long defaultLOC = 20;
  String defaultSource = "//I'm a source file";
  Submission emptySubmission = new Submission();
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for Submission constructor.
   */
  @Test
  public final void testSubmission() {
    Submission s1 = new Submission();
    assertEquals("", s1.getName());
    assertEquals(0, s1.getLOC());
    Iterator<Token> aItr = s1.tIterator();
    assertFalse(aItr.hasNext());
    assertEquals(s1, emptySubmission);
    assertEquals(s1.hashCode(), emptySubmission.hashCode());
    assertEquals(s1.toString(), emptySubmission.toString());
  }
  
  /**
   * Test method for Submission overloaded constructor.
 * @throws IOException 
   */
  @Test
  public final void testSubmissionStringFile() throws IOException {
	File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	file.getParentFile().mkdirs();
	FileWriter writer = new FileWriter(file.getAbsoluteFile());
	file.deleteOnExit();
    writer.write("\"I'm test file 1\n\"");
    writer.write("\"I'm a second line of code\n\"");
    writer.write("\"I'm a third line of code\n\"");
    writer.write("\"I'm the last line of code.\"");
    writer.close();
    Submission s1 = new Submission(defaultName, file);
    assertEquals(defaultName, s1.getName());
    assertEquals(4, s1.getLOC());
    assertEquals(s1.getTokens().size(), 4);
    assertNotEquals(s1, emptySubmission);
    assertNotEquals(s1.hashCode(), emptySubmission.hashCode());
    assertNotEquals(s1.toString(), emptySubmission.toString());
  }

  
  /**
   * Test method for Submission#setId.
 * @throws IOException 
   */
  @Test
  public final void testSetName() throws IOException {
	File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	file.getParentFile().mkdirs();
	FileWriter writer = new FileWriter(file.getAbsoluteFile());
	file.deleteOnExit();
	writer.write("I'm test file 1");
	writer.close();
    Submission s1 = new Submission(defaultName, file);
    s1.setName("Another Name");
    assertEquals(1, s1.getLOC());
    assertEquals("I'm test file 1", s1.getSource());
    assertEquals("Another Name", s1.getName());
    assertNotEquals(s1, emptySubmission);
    assertNotEquals(s1.hashCode(), emptySubmission.hashCode());
    String SubmissionStr = s1.toString();
    assertTrue (SubmissionStr.contains("Another Name"));
  }
  
  /**
   * Test method for Submission#setLOC.
 * @throws IOException 
   */
  @Test
  public final void testSetLOC() throws IOException {
	File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	file.getParentFile().mkdirs();
	FileWriter writer = new FileWriter(file.getAbsoluteFile());
	file.deleteOnExit();
	writer.write("I'm test file 1");
	writer.close();
    Submission s1 = new Submission(defaultName, file);
    s1.setLOC(500);
    assertEquals("I'm test file 1", s1.getSource());
    assertEquals(defaultName, s1.getName());
    assertEquals(500, s1.getLOC());
    assertNotEquals(s1, emptySubmission);
    assertNotEquals(s1.hashCode(), emptySubmission.hashCode());
    String SubmissionStr = s1.toString();
    assertTrue (SubmissionStr.contains(defaultName));
  }
  
  /**
   * Test method for Submission#setId.
 * @throws IOException 
   */
  @Test
  public final void testSetSource() throws IOException {
	File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	file.getParentFile().mkdirs();
	FileWriter writer = new FileWriter(file.getAbsoluteFile());
	file.deleteOnExit();
	writer.write("I'm test file 1");
	writer.close();
    Submission s1 = new Submission(defaultName, file);
    s1.setSource("Another Source");
    assertEquals("Another Source", s1.getSource());
    assertEquals(defaultName, s1.getName());
    assertEquals(1, s1.getLOC());
    assertNotEquals(s1, emptySubmission);
    assertNotEquals(s1.hashCode(), emptySubmission.hashCode());
    String SubmissionStr = s1.toString();
    assertTrue (SubmissionStr.contains(defaultName));
  }
 
  /**
   * Test method for Submission#setTokens.
   * @throws IOException 
   */  
  @Test
  public final void testSetTokens() throws IOException {
	  File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	  file.getParentFile().mkdirs();
	  FileWriter writer = new FileWriter(file.getAbsoluteFile());
	  file.deleteOnExit();
	  writer.write("I'm test file 1");
	  writer.close();
	  Token test1 = new Token(TokenEnum.STRING_LITERAL, "test 1", false);
	  Token test2 = new Token(TokenEnum.STRING_LITERAL, "test 2", true);
	  List<Token> testTokens = Arrays.asList(test1, test2);
	  Submission s1 = new Submission(defaultName, file);
	  s1.setTokens(testTokens);
	  testTokens = s1.getTokens();
	  assertEquals(test1, testTokens.get(0));
	  assertEquals(test2, testTokens.get(1));
	  assertEquals(2,testTokens.size());
	  assertEquals("test 1", test1.getLexeme());
	  assertEquals("test 2", test2.getLexeme());
	  assertEquals(false, test1.getIsJava());
	  assertEquals(true, test2.getIsJava());
  }

  /**
   * Test method for Submission#addToken and Submission list.
   * @throws IOException 
   */
  @Test
  public final void testAddToken() throws IOException {
	  File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	  file.getParentFile().mkdirs();
	  FileWriter writer = new FileWriter(file.getAbsoluteFile());
	  file.deleteOnExit();
	  writer.write("I'm test file 1");
	  writer.close();
	  Token t1 = new Token(TokenEnum.STRING_LITERAL, "phase 1", false);
	  Token t2 = new Token(TokenEnum.STRING_LITERAL, "phase 2", true);
	  Submission s1 = new Submission(defaultName, file);
	  List<Token> tempTokens = s1.getTokens();
	  tempTokens.clear();
	  s1.setTokens(tempTokens);
	  s1.addToken(t1);
	  s1.addToken(t2);
	  tempTokens = s1.getTokens();
	  
	  assertEquals(t1,tempTokens.get(0));
	  assertEquals(t2,tempTokens.get(1));
	  assertEquals(2,tempTokens.size());
	  assertEquals("phase 1",t1.getLexeme());
	  assertEquals("phase 2",t2.getLexeme());
	  assertEquals(false, t1.getIsJava());
	  assertEquals(true, t2.getIsJava());
	  
	  s1.addToken(t2);
	  assertEquals(3,tempTokens.size());
  }
  
  /**
   * Test method for Submission#clone}.
   * @throws IOException 
   */

  @Test
  public final void testClone() throws IOException {
	File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
    file.getParentFile().mkdirs();
    FileWriter writer = new FileWriter(file.getAbsoluteFile());
    file.deleteOnExit();
	writer.write("I'm test file 1");
	writer.close();
    Submission s1 = new Submission(defaultName,file);
    Submission s2 = (Submission)s1.clone();
    assertEquals(defaultName, s2.getName());
    assertEquals(1, s2.getLOC());
    assertEquals(s1, s2);
    assertEquals(s1.hashCode(), s2.hashCode());
    String SubmissionStr = s2.toString();
    assertEquals (s1.toString(), SubmissionStr);
    assertEquals(s1.getTokens().size(), s2.getTokens().size());
  }
}