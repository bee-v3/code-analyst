package edu.odu.cs.cs350;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestStudent {


  String defaultName = "a name";
  boolean defaultSuspicious = false;
  Student emptyStudent = new Student();
  int defaultId = 5;

  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for student constructor.
   */
  @Test
  public final void testStudent() {
    Student s1 = new Student();
    assertEquals("", s1.getName());
    assertEquals(false, s1.getSuspicious());
    Iterator<Submission> aItr = s1.aIterator();
    assertFalse(aItr.hasNext());
    assertEquals(s1, emptyStudent);
    assertEquals(s1.hashCode(), emptyStudent.hashCode());
    assertEquals(s1.toString(), emptyStudent.toString());
  }

  /**
   * Test method for {@link Student#generateSubmissions(String, String)}.
 * @throws IOException 
   */
  @Test
  public final void testGenerateSubmissions() throws IOException {
    File file = new File("./src/test/data/studentsubmissions/studentTest", "testFile1.cpp");
    file.getParentFile().mkdirs();
    File file2 = new File("./src/test/data/studentsubmissions/studentTest", "testFile2.java");
    file2.getParentFile().mkdirs();
    File file3 = new File("./src/test/data/studentsubmissions/studentTest/nested", "testFile1.cpp");
    file3.getParentFile().mkdirs();
    
    FileWriter writer = new FileWriter(file.getAbsoluteFile());
    writer.write("\"I'm test file 1\"");
    writer.close();
    FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
    writer2.write("\"I'm test file 2\"");
    writer2.close();
    FileWriter writer3 = new FileWriter(file3.getAbsoluteFile());
    writer3.write("\"I'm test file 3\"");
    writer3.close();

    
    String filepath = file.getAbsoluteFile().getParent();
    
    Student s1 = new Student("SubmissionGeneration");
    s1.generateSubmissions(filepath);
    Submission su1 = new Submission();
    
    List<Submission> tempList = s1.getSubmissions();
    assertEquals(3,tempList.size());
    su1 = tempList.get(0);   
    assertEquals(su1.getTokens().get(0).getLexeme(), "I'm test file 1");
    assertEquals(su1.getName(), "testFile1.cpp");
    su1 = tempList.get(1);
    assertEquals(su1.getTokens().get(0).getLexeme(), "I'm test file 3");
    assertEquals(su1.getName(), "testFile1.cpp");
    su1 = tempList.get(2);
    assertEquals(su1.getTokens().get(0).getLexeme(), "I'm test file 2");
    assertEquals(su1.getName(), "testFile2.java");
    file.deleteOnExit();
    file2.deleteOnExit();
    file3.deleteOnExit();
  }
  
  /**
   * Test method for Student#feedbackOutput()
 * @throws IOException 
   */
  @Test
  public final void testFeedbackOutput() throws IOException {
	  File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
		file.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(file.getAbsoluteFile());
		file.deleteOnExit();
	    writer.write("I'm test file 1\n");
	    writer.write("I'm a second line of code\n");
	    writer.write("I'm a third line of code\n");
	    writer.write("I'm the last line of code.");
	    writer.close();
	    Submission s1 = new Submission("testFile1.cpp", file); 
	    File file2 = new File("./src/test/data/studentsubmissions/studentTest2", "testFile2.java");
		FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
	    writer2.write("I'm test file 2\n");
	    writer2.write("I'm a second line of code\n");
	    writer2.write("I'm a third line of code\n");
	    writer2.write("I'm the last line of code.");
	    writer2.close();
	    Submission s2 = new Submission("testFile2.cpp", file2); 
	    Student testStudent = new Student("tstudent001");
	    testStudent.addSubmission(s1);
	    testStudent.addSubmission(s2);
	    assertEquals(testStudent.getName(), "tstudent001");
	    assertEquals(testStudent.getSubmissions().size(), 2);
	    List<Submission> tempList;
	    tempList = testStudent.getSubmissions();
	    long v1, v2;
	    v1 = tempList.get(0).getLOC();
	    v2 = tempList.get(1).getLOC();
	    long totalLines = 0;
	    totalLines = totalLines + v1 + v2;
	    assertEquals(totalLines, 8);
	    file.deleteOnExit();
	    file2.deleteOnExit();
  }

  /**
   * Test method for {@link Student#setName(String)}.
   */
  @Test
  public final void testSetName() {
    Student s1 = new Student(defaultName);
    s1.setName("another name");
    assertEquals("another name", s1.getName());
    assertEquals(false, s1.getSuspicious());
    assertNotEquals(s1, emptyStudent);
    assertNotEquals(s1.hashCode(), emptyStudent.hashCode());
    String studentStr = s1.toString();
    assertTrue (studentStr.contains("another name"));
  }

  
  /**
   * Test method for {@link Student#setSuspicious(boolean)}.
   */
  @Test
  public final void testSetSuspicious() {
    Student s1 = new Student(defaultName);
    s1.setSuspicious(true);
    assertEquals(defaultName, s1.getName());
    assertEquals(true, s1.getSuspicious());
    assertNotEquals(s1, emptyStudent);
    assertNotEquals(s1.hashCode(), emptyStudent.hashCode());
    String studentStr = s1.toString();
    assertTrue (studentStr.contains(defaultName));
  }
  
  /**
   * Test method for {@link Student#getSubmissions()}.
 * @throws IOException 
   */
  @Test
  public final void testSetSubmissions() throws IOException {
	  File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	  file.getParentFile().mkdirs();
	  File file2 = new File("./src/test/data/studentsubmissions/studentTest2", "testFile2.java");
	  file2.getParentFile().mkdirs();
	  file.deleteOnExit();
	  file2.deleteOnExit();
	  FileWriter writer = new FileWriter(file.getAbsoluteFile());
	  writer.write("I'm test file 1");
	  writer.close();
	  FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
	  writer2.write("I'm test file 2");
	  writer2.close();
	  Submission test1 = new Submission("testsub 1", file);
	  Submission test2 = new Submission("testsub 2", file2);
	  List<Submission> testSubmissions = Arrays.asList(test1, test2);
	  Student s1 = new Student(defaultName);
	  s1.setSubmissions(testSubmissions);
	  testSubmissions = s1.getSubmissions();
	  assertEquals(test1, testSubmissions.get(0));
	  assertEquals(test2, testSubmissions.get(1));
	  assertEquals(2,testSubmissions.size());
	  assertEquals("testsub 1", test1.getName());
	  assertEquals("testsub 2", test2.getName());
  }

  /**
   * Test method for Student#addSubmission and Submission list.
 * @throws IOException 
   */
  @Test
  public final void testAddSubmission() throws IOException {
	  File file = new File("./src/test/data/studentsubmissions/studentTest2", "testFile1.cpp");
	  file.getParentFile().mkdirs();
	  File file2 = new File("./src/test/data/studentsubmissions/studentTest2", "testFile2.cpp");
	  file2.getParentFile().mkdirs();
	  file.deleteOnExit();
	  file2.deleteOnExit();
	  FileWriter writer = new FileWriter(file.getAbsoluteFile());
	  writer.write("I'm test file 1");
	  writer.close();
	  FileWriter writer2 = new FileWriter(file2.getAbsoluteFile());
	  writer2.write("I'm test file 2");
	  writer2.close();
	  Submission su1 = new Submission("testsub1", file);
	  Submission su2 = new Submission("testsub2", file2);
	  Student s1 = new Student(defaultName);
	  List<Submission> tempList;
	  tempList = s1.getSubmissions();
	  tempList.clear();
	  s1.setSubmissions(tempList);
	  s1.addSubmission(su1);
	  s1.addSubmission(su2);
	  tempList = s1.getSubmissions();
	  
	  assertEquals(su1,tempList.get(0));
	  assertEquals(su2,tempList.get(1));
	  assertEquals(2,tempList.size());
	  assertEquals("testsub1", su1.getName());
	  assertEquals("testsub2", su2.getName());
	  assertEquals(2,tempList.size());
  }

  /**
   * Test method for Student#clone}.
   */
  @Test
  public final void testClone() {
    Student s1 = new Student(defaultName);
    Student s2 = (Student)s1.clone();
    assertEquals(defaultName, s2.getName());
    assertEquals(defaultSuspicious, s2.getSuspicious());
    assertEquals(s1, s2);
    assertEquals(s1.hashCode(), s2.hashCode());
    String StudentStr = s2.toString();
    assertEquals (s1.toString(), StudentStr);
    assertEquals(s1.getSubmissions().size(), s2.getSubmissions().size());
  }

}
//