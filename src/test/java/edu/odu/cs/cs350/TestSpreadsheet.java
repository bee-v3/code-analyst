package edu.odu.cs.cs350;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class TestSpreadsheet {

	String defaultSheetName = "test";		//Default spreadsheet name.
	List<String> firstStudents = Arrays.asList("zdevo1","zdevo2","zdevo3"); //Test data for student1 list.
	List<String> secondStudents = Arrays.asList("mdevo1","mdevo2","mdevo3"); //Test data for student2 list.
	List<Double> rawScores = Arrays.asList(22.56,26.32,32.54); //Test data for raw scores list.
	List<Double> zScores = Arrays.asList(55.12, 63.23, 66.11); //Test data for z scores list.
	Spreadsheet emptySpreadsheet = new Spreadsheet(); //Empty spreadsheet object for error checking.
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for {@link Spreadsheet#Spreadsheet()}
   */
  
  @Test
  public final void testSpreadsheet() {
	    Spreadsheet s1 = new Spreadsheet();
	    Spreadsheet s2 = (Spreadsheet)s1.clone();
	    assertEquals("Report", s2.getTemplateName()); //Is the sheet name the correct default name?
	    assertTrue(s2.getStudent1().isEmpty()); //All lists should be empty.
	    assertTrue(s2.getStudent2().isEmpty());
	    assertTrue(s2.getRawScore().isEmpty());
	    assertTrue(s2.getZScore().isEmpty());
	    assertEquals(s1, s2); //Cloned spreadsheets should be the same.
	    assertEquals(s2, emptySpreadsheet); //The cloned spreadsheet should also be empty.
	    assertEquals(s1.hashCode(), s2.hashCode()); //Hash codes should be identical between original and cloned spreadsheets.
	    String SpreadsheetStr = s2.toString();
	    assertEquals (s1.toString(), SpreadsheetStr); //Cloned spreadsheet and original should have the same data in their string buffers.
  }

  /**
   * Test method for {@link Spreadsheet#Spreadsheet(List, List, List, List)}
   */
  @Test
  public final void testSpreadsheetListListListList() {
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  Spreadsheet s2 = (Spreadsheet)s1.clone();
	  assertEquals("Report", s2.getTemplateName()); //Spreadsheet name was not changed, should still be default.
	  assertEquals(s2.getStudent1(), firstStudents); 
	  assertEquals(s2.getStudent2(), secondStudents);
	  assertEquals(s2.getRawScore(), rawScores);
	  assertEquals(s2.getZScore(), zScores);
	  assertEquals(s1, s2);
	  assertEquals(s1.hashCode(), s2.hashCode());
	  assertNotEquals(s2, emptySpreadsheet);
	  String SpreadsheetStr = s2.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);     
  }

  /**
   * Test method for {@link Spreadsheet#setTemplateName(String)}
   */
  @Test
  public final void testsetTemplateName() {
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  s1.setTemplateName("anotherSheet");
	  assertEquals("anotherSheet", s1.getTemplateName());
	  assertEquals(s1.getStudent1(), firstStudents);
	  assertEquals(s1.getStudent2(), secondStudents);
	  assertEquals(s1.getRawScore(), rawScores);
	  assertEquals(s1.getZScore(), zScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#setOutputPath(String)}
   */
  @Test
  public final void testSetOutputPath() {
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  s1.setOutputPath("C://AnotherPath/");
	  assertEquals("C://AnotherPath/", s1.getOutputPath());
	  assertEquals(s1.getTemplateName(), "Report");
	  assertEquals(s1.getStudent1(), firstStudents);
	  assertEquals(s1.getStudent2(), secondStudents);
	  assertEquals(s1.getRawScore(), rawScores);
	  assertEquals(s1.getZScore(), zScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#setStudent1(List)}
   */
  @Test
  public final void testSetStudent1() {
	  
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  List<String> differentStudent1 = Arrays.asList("diff1","diff2","diff3");
	  s1.setStudent1(differentStudent1);
	  assertEquals(differentStudent1, s1.getStudent1());
	  assertEquals("Report", s1.getTemplateName());
	  assertEquals(s1.getStudent2(), secondStudents);
	  assertEquals(s1.getRawScore(), rawScores);
	  assertEquals(s1.getZScore(), zScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#setStudent2(List)}
   */
  @Test
  public final void testSetStudent2() {
	  
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  List<String> differentStudent2 = Arrays.asList("diff1","diff2","diff3");
	  s1.setStudent2(differentStudent2);
	  assertEquals(differentStudent2, s1.getStudent2());
	  assertEquals("Report", s1.getTemplateName());
	  assertEquals(s1.getStudent1(), firstStudents);
	  assertEquals(s1.getRawScore(), rawScores);
	  assertEquals(s1.getZScore(), zScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#setRawScore(List)}
   */
  @Test
  public final void testSetRawScore() {
	  
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  List<Double> differentRawScore = Arrays.asList(101.11,105.22,111.33);
	  s1.setRawScore(differentRawScore);
	  assertEquals(differentRawScore, s1.getRawScore());
	  assertEquals("Report", s1.getTemplateName());
	  assertEquals(s1.getStudent1(), firstStudents);
	  assertEquals(s1.getStudent2(), secondStudents);
	  assertEquals(s1.getZScore(), zScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#setZScore(List)}.
   */
  @Test
  public final void testSetZScore() {
	  
	  Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
	  List<Double> differentZScore = Arrays.asList(11.11,15.22,11.33);
	  s1.setZScore(differentZScore);
	  assertEquals(differentZScore, s1.getZScore());
	  assertEquals("Report", s1.getTemplateName());
	  assertEquals(s1.getStudent1(), firstStudents);
	  assertEquals(s1.getStudent2(), secondStudents);
	  assertEquals(s1.getRawScore(), rawScores);
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  
  /**
   * Test method for {@link Spreadsheet#addStudent1s(String)} 
   */
  @Test
  public final void testAddStudent1() {
	  String oneStudent = "aStudent";
	  String twoStudent = "anotherStudent";
	  Spreadsheet s1 = new Spreadsheet();
	  s1.addStudent1s(oneStudent);
	  s1.addStudent1s(twoStudent);
	  s1.addStudent1s(oneStudent);  //Duplicates should not be added to the list.
	  s1.addStudent1s(twoStudent);
	  assertEquals(2, s1.getStudent1().size()); //Since duplicates are not supposed to be added to the list, only two things should be in this list.
	  assertEquals(oneStudent, s1.getStudent1().get(0)); //Checking first element of list for equivalence.
	  assertEquals(twoStudent, s1.getStudent1().get(1)); //Checking the second.
	  assertEquals("Report", s1.getTemplateName());
	  assertTrue(!s1.getStudent1().isEmpty()); //We put things in this list. It should not be empty.
	  assertTrue(s1.getStudent2().isEmpty()); //Everything else should be empty.
	  assertTrue(s1.getRawScore().isEmpty());
	  assertTrue(s1.getZScore().isEmpty());
	  assertNotEquals(s1, emptySpreadsheet); //Not all members are empty, so this should not be true.
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#addStudent2s(String)}
 * @throws IOException 
   */
  @Test
  public final void testAddStudent2() throws IOException {
	  String oneStudent = "aStudent";
	  String twoStudent = "anotherStudent";
	  Spreadsheet s1 = new Spreadsheet();
	  s1.addStudent2s(oneStudent);
	  s1.addStudent2s(twoStudent);
	  s1.addStudent2s(oneStudent);
	  s1.addStudent2s(twoStudent);
	  assertEquals(2, s1.getStudent2().size());
	  assertEquals(oneStudent, s1.getStudent2().get(0));
	  assertEquals(twoStudent, s1.getStudent2().get(1));
	  assertEquals("Report", s1.getTemplateName());
	  assertTrue(s1.getStudent1().isEmpty());
	  assertTrue(!s1.getStudent2().isEmpty());
	  assertTrue(s1.getRawScore().isEmpty());
	  assertTrue(s1.getZScore().isEmpty());
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#addRawScores(Double)}
   */
  @Test
  public final void testAddRawScore(){
	  Double oneRS = 11.11;
	  Double twoRS = 22.22;
	  Spreadsheet s1 = new Spreadsheet();
	  s1.addRawScores(oneRS);
	  s1.addRawScores(twoRS);
	  assertEquals(2, s1.getRawScore().size());
	  assertEquals(oneRS, s1.getRawScore().get(0));
	  assertEquals(twoRS, s1.getRawScore().get(1));
	  assertEquals("Report", s1.getTemplateName());
	  assertTrue(s1.getStudent1().isEmpty());
	  assertTrue(s1.getStudent2().isEmpty());
	  assertTrue(!s1.getRawScore().isEmpty());
	  assertTrue(s1.getZScore().isEmpty());
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }
  /**
   * Test method for {@link Spreadsheet#addZScores(Double)}
   */
  @Test
  public final void testAddZScore(){
	  Double oneRS = 11.11;
	  Double twoRS = 22.22;
	  Spreadsheet s1 = new Spreadsheet();
	  s1.addZScores(oneRS);
	  s1.addZScores(twoRS);
	  assertEquals(2, s1.getZScore().size());
	  assertEquals(oneRS, s1.getZScore().get(0));
	  assertEquals(twoRS, s1.getZScore().get(1));
	  assertEquals("Report", s1.getTemplateName());
	  assertTrue(s1.getStudent1().isEmpty());
	  assertTrue(s1.getStudent2().isEmpty());
	  assertTrue(s1.getRawScore().isEmpty());
	  assertTrue(!s1.getZScore().isEmpty());
	  assertNotEquals(s1, emptySpreadsheet);
	  String SpreadsheetStr = s1.toString();
	  assertEquals (s1.toString(), SpreadsheetStr);
  }

  /**
   * Test method for Spreadsheet#generateSpreadsheet.
   */

  @Test
  public final void testGenerateSpreadsheet() throws IOException {
	  Spreadsheet testsheet = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores); //Giving the spreadsheet the information it needs.
	  testsheet.setTemplateName("testSheet"); //Setting the spreadsheet name to whatever we want.
	  assertEquals(testsheet.getStudent1().size(), testsheet.getStudent2().size());
	  assertEquals(testsheet.getStudent1().size(), testsheet.getRawScore().size());
	  assertEquals(testsheet.getStudent1().size(), testsheet.getZScore().size());
	  
	  testsheet.generateSheet(); //Generating the spreadsheet. The spreadsheet should be viewed for accuracy.
  }
  
  /**
   * Test method for Spreadsheet#Clone.
   */
  @Test
  public final void testClone() {
    Spreadsheet s1 = new Spreadsheet(firstStudents, secondStudents, rawScores, zScores);
    Spreadsheet s2 = (Spreadsheet)s1.clone();
    assertEquals(s1.getTemplateName(), s2.getTemplateName());
    assertEquals(s1.getStudent1(), s2.getStudent1());
    assertEquals(s1.getStudent2(), s2.getStudent2());
    assertEquals(s1.getRawScore(), s2.getRawScore());
    assertEquals(s1.getZScore(), s2.getZScore());
    assertEquals(s1, s2);
    assertEquals(s1.hashCode(), s2.hashCode());
    String StudentStr = s2.toString();
    assertEquals (s1.toString(), StudentStr);
  }
}

