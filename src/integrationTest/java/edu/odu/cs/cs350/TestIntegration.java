package edu.odu.cs.cs350;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class TestIntegration {

	/**
	 * Tests the integration between Submission and Token classes using an outside
	 * file to gather tokens from.
	 */
	@Test
	public void testSubmissionAndToken() {
		try {
			File testFile = new File("./src/integrationTest/data/ajones", "q2.cpp");
			BufferedReader testReader = new BufferedReader(new FileReader(testFile));
			List<Token> t1 = new ArrayList<Token>();

			try {
				CppScanner scanner = new CppScanner(testReader);
				Token token = scanner.yylex();
				while (token.getType() != TokenEnum.EOF) {
					t1.add(token);
					token = scanner.yylex();
				}
				testReader.close();

				Submission testSubmission = new Submission(testFile.getName(), testFile);

				List<Token> t2 = testSubmission.getTokens();
				assertEquals(t1.size(), t2.size());

				int tokenCount2 = 0;

				for (Token s : t2) {
					assertThat(s.equals(t2.get(tokenCount2)), is(true));

					tokenCount2++;
				}

				Submission testClonedSubmission = (Submission) testSubmission.clone();

				List<Token> t2Clone = testClonedSubmission.getTokens();
				assertThat(t2.size(), is(t2Clone.size()));

				int tokenCount = 0;

				for (Token s : t2) {
					assertThat(s.equals(t2Clone.get(tokenCount)), is(true));
					tokenCount++;
				}

			} catch (IOException e) {
				fail();
			}
		} catch (FileNotFoundException e) {
			fail();
		}

	}

	/**
	 * Tests integration between Student and Submission class
	 */
	@Test
	public void testStudentAndSubmission() {
		File testDirectory = new File("./src/integrationTest/data/ajones");

		Student testStudent = new Student(testDirectory.getName());
		assertThat(testStudent.getName(), is("ajones"));

		File testFile = new File("./src/integrationTest/data/ajones", "q2.cpp");

		try {
			Submission testSubmission = new Submission(testFile.getName(), testFile);
			assertThat(testSubmission.getTokens().size(), is(34));

			testStudent.addSubmission(testSubmission);
			assertThat(testStudent.getSubmissions().size(), is(1));

			Submission testSubmission2 = (Submission) testSubmission.clone();
			assertThat(testSubmission.equals(testSubmission2), is(true));
			assertThat(testSubmission.hashCode(), is(testSubmission2.hashCode()));

			testStudent.addSubmission(testSubmission2);
			assertThat(testStudent.getSubmissions().size(), is(1));

			Student testStudent2 = (Student) testStudent.clone();
			assertThat(testStudent2.equals(testStudent), is(true));
			assertThat(testStudent2.getSubmissions().size(), is(1));
			assertThat(testStudent2.getSubmissions().get(0).getTokens().size(), is(34));
		} catch (IOException e) {
			fail();
		}
	}

	/**
	 * Tests integration between Instructor and Student class
	 */
	@Test
	public void testInstructorAndStudent() {
		String[] testArgs = { "./src/integrationTest/data/", "." };
		Instructor testInstructor = new Instructor(testArgs);
		List<String> studentNames = testInstructor.getStudentDirectoryNames(testInstructor.getAssignmentDirectory());
		testInstructor.getStudents(studentNames);
		assertThat(testInstructor.getStudentList().size(), is(3));

		List<Student> studentList1 = testInstructor.getStudentList();
		StringBuffer names = new StringBuffer();
		for (Student s : studentList1) {
			names.append(s.getName());
			names.append(" ");
			assertThat(s.getSubmissions().size(), is(1));
		}
		assertThat(names.toString().contains("ajones"), is(true));
		assertThat(names.toString().contains("jsmith"), is(true));
		assertThat(names.toString().contains("tlee"), is(true));
		assertThat(names.toString().contains("ojackson.10aa"), is(false));
		assertThat(names.toString().contains("tlee.98"), is(false));
		assertThat(names.toString().contains("tlee.99"), is(false));

		Instructor testInstructor2 = (Instructor) testInstructor.clone();
		assertThat(testInstructor2.equals(testInstructor), is(true));
		assertThat(testInstructor2.getStudentList().size(), is(studentList1.size()));
	}
}
