package edu.odu.cs.cs350;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

/**
 * Student Class This class contains student information, collections of
 * Submissions based on the student, a collection of tokens based on the
 * student, as well as the means to convert Submissions to tokens.
 */
public class Student implements Cloneable {
	/**
	 * String member that contains the name of the student.
	 */
	private String studentName = ""; // The name of the student.
	/**
	 * Boolean member that is false if the student has a normalized score less than one and true if the student has a normalized score greater than one.
	 */
	private boolean suspicious = false; // Is this student suspected of copying?
	/**
	 * List of submissions containing the student's submissions.
	 */
	private List<Submission> studentSubmissions; // A list of this students submissions.

	/**
	 * Creates a blank student with no name, 0 Id, suspicious flag set to false, and
	 * empty Submission and token lists.
	 */

	public Student() {
		studentName = "";
		suspicious = false;
		studentSubmissions = new ArrayList<Submission>();
	}

	/**
	 * Creates a new student.
	 * 
	 * @param name - name of student.
	 * @param id   - id of student.
	 */

	public Student(String name) {
		studentName = name;
		suspicious = false;
		studentSubmissions = new ArrayList<Submission>();
	}

	/**
	 * Gets the name of a student.
	 * 
	 * @return the name of a student.
	 */
	public String getName() {
		return studentName;
	}

	/**
	 * Sets the name of a student.
	 * 
	 * @Param n - the name of a student to set.
	 */
	public void setName(String n) {
		studentName = n;
	}

	/**
	 * Gets the suspicious flag of a student.
	 * 
	 * @return the suspicious flag of a student.
	 */
	public boolean getSuspicious() {
		return suspicious;
	}

	/**
	 * Sets the suspicious flag of a student.
	 * 
	 * @Param n - the suspicious flag of a student to set.
	 */
	public void setSuspicious(boolean b) {
		suspicious = b;
	}

	/**
	 * Gets the submission list of a student.
	 * 
	 * @return the submission list of a student.
	 */
	public List<Submission> getSubmissions() {
		return studentSubmissions;
	}

	/**
	 * Sets the submission list of a student.
	 * 
	 * @Param n - the submission list of a student to set.
	 */
	public void setSubmissions(List<Submission> b) {
		studentSubmissions = b;
	}

	/**
	 * Add an Submission to the end of a student's Submission list if the Submission
	 * is not currently in the list.
	 * 
	 * @param au - Submission to be added.
	 */
	public void addSubmission(Submission as) {

		boolean found = false;
		for (Submission existingSubmission : studentSubmissions) {
			if (existingSubmission.equals(as)) {
				found = true;
				break;
			}
		}
		if (found == false) {
			studentSubmissions.add(as);
		}

	}

	/**
	 * Converts the files in a directory to submission objects.
	 * 
	 * @param assignmentName      - The name of the assignment.
	 * @param assignmentDirectory - Where the files are located.
	 * @throws IOException
	 */
	public void generateSubmissions(String assignmentDirectory) throws IOException {
		File directory = new File(assignmentDirectory); // Setting a temporary value to use as our root directory.
		if (!directory.exists() || !directory.isDirectory()) {
			return;
		}

		File[] fList = directory.listFiles(); // Creating a list of files of files in the root directory.
		for (File file : fList) { // Loop until the last file in the list.
			if (file.isFile()) { // If the file in the list is actually a file...
				Submission addingSubmission = new Submission(file.getName(), file); // Create a new submission with that
																					// file.
				addSubmission(addingSubmission); // And add that file to our student's submissions list.
			} else if (file.isDirectory()) { // If the file is not a file but is a directory....
				generateSubmissions(file.getPath()); // Run this function again, but use that directory as our new root
														// directory.
			}
		}
	}

	/**
	 * Provides output to the user to show the student's name that is to be to be
	 * compared, the number of files in that student's directory, and the total
	 * lines of code from all the files in the student's directory.
	 */
	public void feedbackOutput() {
		int size = this.studentSubmissions.size();
		int i = 0;
		long totalLOC = 0;
		while (i < size) {
			totalLOC = totalLOC + this.studentSubmissions.get(i).getLOC();
			i++;
		}
		StringBuilder sbuf = new StringBuilder();
		Formatter fmt = new Formatter(sbuf);
		fmt.format("%-15s", this.studentName);
		sbuf.append("Files: ");
		fmt.format("%-3d", size);
		fmt.format("%s", " LOC: ");
		fmt.format("%-5d", totalLOC);
		System.out.print(sbuf.toString());
		fmt.close();
	}

	/**
	 * Renders the student in a string format to contain student name and
	 * identification.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Student Name: ");
		buf.append(studentName);
		return buf.toString();
	}

	/**
	 * Compares two students for equality. Students are the same if the have the
	 * same name and identification.
	 * 
	 * @param obj - object to be compared for equality
	 * @return true if obj is found to be equal
	 */

	public boolean equals(Object obj) {
		if (!(obj instanceof Student)) {
			return false;
		}
		Student other = (Student) obj;
		if (studentName.equals(other.studentName)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the hash code value for this object.
	 * 
	 * @return the hash code value for this student.
	 */
	public int hashCode() {
		return studentName.hashCode();
	}

	/**
	 * Returns a deep copy of this object.
	 */
	@Override
	public Object clone() {
		Student studentClone = new Student(studentName);
		studentClone.suspicious = this.suspicious;
		for (Submission s : studentSubmissions) {
			studentClone.addSubmission((Submission) s.clone());
		}
		return studentClone;
	}

	/**
	 * Provides access to the list of Submissions.
	 * 
	 * @return iterator over the Submissions.
	 */
	public Iterator<Submission> aIterator() {
		return studentSubmissions.iterator();
	}
}
