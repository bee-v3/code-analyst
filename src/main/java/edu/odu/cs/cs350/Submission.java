package edu.odu.cs.cs350;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Submission Class This class contains Submission information, such as the
 * assignment name of the submission, the ID of the submission (for keeping
 * track of different files of the same assignment), the files of the
 * directories the submission resides in, a string copy of the submission source
 * code, and a list of tokens from the source code.
 */
public class Submission implements Cloneable {
	/**
	 * String member that contains the name of the file this submission represents.
	 */
	private String sourceFileName = ""; 
	/**
	 * Long member that represents how many lines of code are in the source file.
	 */
	private long linesOfCode = 0; 
	/**
	 * File member that contains a local copy of the file passed in to the submission class.
	 */
	private File myFile; 
	/**
	 * String member that contains the contents of the file passed in to the submission class.
	 */
	private String sourceCode = ""; 
	/**
	 * List of tokens that contains the tokens generated from the source file contained in this submission.
	 */
	private List<Token> studentTokens = new ArrayList<Token>(); 

	/**
	 * Creates a blank Submission with no name, 0 Id, an empty sourceCode string,
	 * and an empty token list.
	 */
	public Submission() {
		sourceFileName = "";
		sourceCode = "";
		linesOfCode = 0;
		myFile = new File("");
	}

	/**
	 * Creates a new Submission.
	 * 
	 * @param name   - name of the assignment.
	 * @param id     - id of Submission.
	 * @param inFile - File residing in directory.
	 */

	public Submission(String n, File inFile) throws IOException {
		sourceFileName = n;
		myFile = inFile;

		tokenizeSubmission();

		String path;
		Charset encoding = Charset.defaultCharset(); // The character set we are using for the information we pull out
														// of the file.
		path = myFile.getAbsolutePath(); // Getting the path of the file.

		byte[] encoded = Files.readAllBytes(Paths.get(path)); // Storing all information in the file to an array of
																// bytes.
		sourceCode = new String(encoded, encoding); // Converting the array of bytes using our default character set to
													// a String.

		// Calculates how many lines of code are in the input file.
		try (Stream<String> lines = Files.lines(myFile.toPath(), Charset.defaultCharset())) {
			long numOfLines = lines.count();
			linesOfCode = numOfLines;
		}

	}

	/**
	 * 
	 */
	public void tokenizeSubmission() {
		try {
			String extension;
			int x = myFile.getName().lastIndexOf('.');
			if (x == -1) {
				return;
			}
			extension = myFile.getName().substring(x);
			if (!(extension.equalsIgnoreCase(".cpp") || extension.equalsIgnoreCase(".h")
					|| extension.equalsIgnoreCase(".java"))) {
				return;
			}

			BufferedReader fReader = new BufferedReader(new FileReader(myFile.getAbsolutePath()));
			if (extension.equalsIgnoreCase(".cpp") || extension.equalsIgnoreCase(".h")) {
				try {
					CppScanner scanner = new CppScanner(fReader);
					Token token = scanner.yylex();
					while (token.getType() != TokenEnum.EOF) {
						addToken(token);
						token = scanner.yylex();
					}
				} catch (IOException e) {
				}
			} else {
				try {
					JavaScanner scanner = new JavaScanner(fReader);
					Token token = scanner.yylex();
					while (token.getType() != TokenEnum.EOF) {
						addToken(token);
						token = scanner.yylex();
					}
				} catch (IOException e) {
				}
			}
		} catch (FileNotFoundException e) {
		}
	}

	/**
	 * Gets the id of a Submission.
	 * 
	 * @return the id of a Submission.
	 */
	public String getName() {
		return sourceFileName;
	}

	/**
	 * Sets the id of a Submission.
	 * 
	 * @Param n - the id of a Submission to set.
	 */
	public void setName(String n) {
		sourceFileName = n;
	}

	/**
	 * Gets how many lines of code are in a Submission.
	 * 
	 * @return the number of lines of code are in a Submission.
	 */
	public long getLOC() {
		return linesOfCode;
	}

	/**
	 * Sets how many lines of code are in a submission.
	 * 
	 * @Param n - the number of lines of code to set.
	 */
	public void setLOC(long n) {
		linesOfCode = n;
	}

	/**
	 * @return the myFile
	 */
	public File getMyFile() {
		return myFile;
	}

	/**
	 * @param myFile the myFile to set
	 */
	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	/**
	 * Gets the source code of the submission.
	 * 
	 * @return the source code of the submission.
	 */
	public String getSource() {
		return sourceCode;
	}

	/**
	 * Sets the source code of the submission.
	 * 
	 * @Param n - the source code of the submission to set.
	 */
	public void setSource(String n) {
		sourceCode = n;
	}

	/**
	 * Gets the token list of a submission.
	 * 
	 * @return the token list of a submission.
	 */
	public List<Token> getTokens() {
		return studentTokens;
	}

	/**
	 * Sets the token list of a submission.
	 * 
	 * @Param n - the token list of a submission to set.
	 */
	public void setTokens(List<Token> b) {
		studentTokens = b;
	}

	/**
	 * Add a token to the end of a submissions's token list if the token is not
	 * currently in the list.
	 * 
	 * @param tok - token to be added.
	 */
	public void addToken(Token tok) {
		studentTokens.add(tok);
	}

	/**
	 * Renders the submission in a string format to contain assignment name and
	 * submission identification.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("File: ");
		buf.append(sourceFileName);
		buf.append(" LOC : ");
		buf.append(linesOfCode);
		return buf.toString();
	}

	/**
	 * Compares two Submissions for equality. Submissions are the same if the have
	 * the same assignment name and identification.
	 * 
	 * @param obj - object to be compared for equality
	 * @return true if obj is found to be equal
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof Submission)) {
			return false;
		}
		Submission other = (Submission) obj;
		if (sourceFileName == other.sourceFileName && linesOfCode == other.linesOfCode
				&& sourceFileName == other.sourceFileName) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the hash code value for this object.
	 * 
	 * @return the hash code value for this Submission.
	 */
	public int hashCode() {
		return sourceFileName.hashCode();
	}

	/**
	 * Returns a deep copy of this object.
	 */
	@Override
	public Object clone() {
		Submission submissionClone;
		try {
			submissionClone = new Submission(sourceFileName, myFile);
			submissionClone.sourceFileName = sourceFileName;
			submissionClone.linesOfCode = linesOfCode;
			submissionClone.myFile = myFile;
			submissionClone.sourceCode = sourceCode;
			List<Token> studentTokens2 = new ArrayList<Token>();
			for (Token s : studentTokens) {
				studentTokens2.add((Token) s.clone());
			}
			submissionClone.studentTokens = studentTokens2;
			return submissionClone;

		} catch (IOException e) {
			System.out.println("Failed to create submission.");
		}

		return new Submission();
	}

	/**
	 * Provides access to the list of tokens
	 * 
	 * @return iterator over the tokens
	 */
	public Iterator<Token> tIterator() {
		return studentTokens.iterator();
	}

}
