package edu.odu.cs.cs350;

/**
 * Token Class This class takes a string and converts the string to a series of
 * tokens.
 */
public class Token implements Cloneable {
	/**
	 * String member that holds the tokens.
	 */
	private String lexeme;
	/**
	 * TokenEnum member that holds the type of enumeration.
	 */
	private TokenEnum type;
	/**
	 * Boolean flag signaling if the source code is java or C++.
	 */
	private boolean isJava = false; 
	/**
	 * Integer specifying which line in the file the token was found on.
	 */
	private int line = 0; 
	/**
	 * Integer specifying which column in the file the token was found on.
	 */
	private int column = 0;

	/**
	 * Creates a blank Token with a empty tokenSequence and isJava flag set to
	 * false.
	 */

	public Token() {
		lexeme = "";
		type = TokenEnum.EOF;
		isJava = false;
	}

	/**
	 * Creates a new Token.
	 * 
	 * @param tokentype Enum of corresponding token type
	 * @param javaorcpp Boolean determining whether the Token is a java or C++ token
	 */
	public Token(TokenEnum tokentype, boolean javaorcpp) {
		lexeme = "";
		type = tokentype;
		isJava = javaorcpp;
	}

	/**
	 * Creates a new Token.
	 * 
	 * @param tokentype Enum of corresponding token type
	 * @param value     String value to assign lexeme
	 * @param javaorcpp Boolean determining whether the Token is a java or C++ token
	 */
	public Token(TokenEnum tokentype, String value, boolean javaorcpp) {
		lexeme = value;
		type = tokentype;
		isJava = javaorcpp;
	}

	/**
	 * Creates a new Token
	 * 
	 * @param tokentype Enum of corresponding token type
	 * @param li        Line token was located
	 * @param col       Column token was located
	 * @param javaorcpp Boolean determining whether the Token is a java or C++ token
	 */
	public Token(TokenEnum tokentype, int li, int col, boolean javaorcpp) {
		type = tokentype;
		isJava = javaorcpp;
		line = li;
		column = col;
	}

	/**
	 * Creates a new Token
	 * 
	 * @param tokentype Enum of corresponding token type
	 * @param li        Line token was located
	 * @param col       Column token was located
	 * @param value     String to assign lexeme
	 * @param javaorcpp Boolean determining whether the Token is a java or C++ token
	 */
	public Token(TokenEnum tokentype, int li, int col, String value, boolean javaorcpp) {
		lexeme = value;
		type = tokentype;
		isJava = javaorcpp;
		line = li;
		column = col;
	}

	/**
	 * Returns the value of lexeme
	 * 
	 * @return lexeme String value of Token object
	 */
	public String getLexeme() {
		return lexeme;
	}

	/**
	 * Assigns lexme a value
	 * 
	 * @param val Incoming string to assign lexeme to
	 */
	public void setLexemme(String val) {
		lexeme = val;
	}

	/**
	 * Returns the value of type
	 * 
	 * @return type Type of token
	 */
	public TokenEnum getType() {
		return type;
	}

	/**
	 * Sets the token type
	 * 
	 * @param typeset Incoming token type to assign type to.
	 */
	public void setType(TokenEnum typeset) {
		type = typeset;
	}

	/**
	 * Gets the type of token.
	 * 
	 * @return the type of code .. false for c++, true for Java.
	 */
	public boolean getIsJava() {
		return isJava;
	}

	/**
	 * Sets the language type.
	 * 
	 * @Param b the language type .. false for c++, true for Java.
	 */
	public void setisJava(boolean b) {
		isJava = b;
	}

	/**
	 * @return the line
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @param line the line to set
	 */
	public void setLine(int line) {
		this.line = line;
	}

	/**
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * @param column the column to set
	 */
	public void setColumn(int column) {
		this.column = column;
	}

	/**
	 * Renders the Token in a string format to contain tokens.
	 */
	public String toString() {
		if (getLexeme().length() > 0) {
			return getType() + ":" + getLexeme();
		} else {
			return getType().toString();
		}

	}

	/**
	 * Compares two Tokens for equality. Tokens are the same if the have the same
	 * tokens.
	 * 
	 * @param obj object to be compared for equality
	 * @return true if obj is found to be equal
	 */
	public boolean equals(Object obj) {
		Token other = (Token) obj;
		if ((other.isJava == isJava) && (other.type == type) && (other.lexeme == lexeme) && (other.line == line)
				&& (other.column == column)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the hash code value for this object.
	 * 
	 * @return the hash code value for this Token.
	 */
	public int hashCode() {
		return lexeme.hashCode() * type.ordinal() * column * line;
	}

	/**
	 * Returns a deep copy of this object.
	 */
	@Override
	public Object clone() {
		Token TokenClone = new Token();
		TokenClone.setisJava(isJava);
		TokenClone.setLexemme(lexeme);
		TokenClone.setType(type);
		TokenClone.setColumn(column);
		TokenClone.setLine(line);
		return TokenClone;
	}

}