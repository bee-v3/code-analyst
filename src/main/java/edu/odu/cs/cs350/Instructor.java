package edu.odu.cs.cs350;

import edu.odu.cs.cs350.codeCompCommon.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Instructor Class
 * <p>
 * This class parses the input parameters and searches student directories
 */
public class Instructor implements Cloneable {
	/**
	 * Default sheet name used to identify where to place report data scores.
	 */
	public static final String DEFAULT_SHEET_NAME = "Report";

	/**
	 * Default constructor
	 * <p>
	 * Sets isError to false by default, assuming no parameters passed.
	 */
	public Instructor() {
		setIsError(true);
	}

	/**
	 * Constructor that initializes Instructor object with user arguments.
	 * 
	 * @param initArgs String array of parameters.
	 */
	public Instructor(String[] initArgs) {
		parseArguments(initArgs);
	}

	/**
	 * Copy constructor.
	 * 
	 * @param toBeCopied Instructor object used to initialize a new Instructor
	 *                   object.
	 */
	public Instructor(Instructor toBeCopied) {
		this.assignmentDirectory = toBeCopied.assignmentDirectory;
		this.outputDirectory = toBeCopied.outputDirectory;
		this.templateName = toBeCopied.templateName;
		this.reportSheetName = toBeCopied.reportSheetName;
		this.isError = toBeCopied.isError;
		this.isHelp = toBeCopied.isHelp;
		this.customTemplate = toBeCopied.customTemplate;
		this.scoredStudentOne = toBeCopied.scoredStudentOne;
		this.rawScores = toBeCopied.rawScores;
		this.normalizedScores = toBeCopied.normalizedScores;
		List<Student> studentList2 = new ArrayList<Student>();
		for (Student s : toBeCopied.studentList) {
			studentList2.add((Student) s.clone());
		}
		this.studentList = studentList2;
	}

	/**
	 * Parses the arguments set by the user to determine validity.
	 * 
	 * @param args1 Array of options declared by the user.
	 */
	public void parseArguments(String[] args1) {
		int argsLength = args1.length;
		setReportSheetName(DEFAULT_SHEET_NAME);

		// Check for no parameters or invalid number of parameters.
		if ((argsLength == 0) || (argsLength > 6) || (argsLength == 3) || (argsLength == 5)) {
			setIsError(true);
			return;
		}

		// Check if user passed in -help
		if (argsLength == 1) {
			if (args1[0] == "-help") {
				setIsHelp(true);
			} else {
				setIsError(true);
			}
			return;
		}

		// Parse arguments. Above section should catch catastrophic argument errors.
		setOutputDirectory(args1[argsLength - 1]);
		setAssignmentDirectory(args1[argsLength - 2]);
		if (argsLength > 2) {
			for (int i = 0; i < argsLength - 2; i += 2) {
				if (args1[i] == "-template" && args1[i + 1] != "") {
					setTemplateName(args1[i + 1]);
					setCustomTemplate(true);
				} else if (args1[i] == "-report" && args1[i + 1] != "") {
					setReportSheetName(args1[i + 1]);

				} else {
					setIsError(true);
					return;
				}
			}
		}
	}

	/**
	 * Collects student names
	 * 
	 * @param assignDir Directory of student submissions.
	 * @return studentDirectory List of student folders to gather submissions from
	 */
	public List<String> getStudentDirectoryNames(String assignDir) {

		File dir = new File(assignDir);

		if (!dir.exists() || !dir.isDirectory()) {
			System.out.println("Invalid assignment directory!\n");
			setIsError(true);
			return null;
		}

		List<File> studentParentFolder = new ArrayList<File>(Arrays.asList(dir.listFiles(File::isDirectory))); // temporary
																												// list
																												// of
																												// directory
																												// names.

		if (studentParentFolder.isEmpty()) {
			System.out.println("Empty assignment directory!");
			setIsError(true);
			return null;
		}

		// directory names converted to strings into a temporary list
		List<String> tempstudentDirectory = new ArrayList<String>();
		for (File l : studentParentFolder) {
			String tempn = l.getName();
			tempstudentDirectory.add(tempn);
		}

		List<String> studentDirectory = tempstudentDirectory.stream().filter(x -> !x.contains("."))
				.collect(Collectors.toList()); // list where final names will be stored.
		tempstudentDirectory.removeIf(x -> !x.contains("."));

		for (String x : tempstudentDirectory) // Go through every name in temporary list, which should now only contain
												// directories with periods in their name.
		{
			String studentName = x.substring(0, x.indexOf('.'));
			boolean isInList = studentDirectory.stream().anyMatch(s -> s.contains(studentName)); // set true if the best
																									// folder version
																									// for that student
																									// has already been
																									// added to
																									// studentDirectory

			if (!isInList) // Check to see if the student isn't already on the final list
			{
				try { // Protection against folders with abnormal version numbers
					int version = Integer.parseInt(x.substring(x.indexOf('.') + 1, x.length())); // Convert everything
																									// past the file
																									// name's period to
																									// an integer for
																									// comparison

					for (String y : tempstudentDirectory) {
						if (y.contains(studentName)) {
							try {
								int v2 = Integer.parseInt(y.substring(y.indexOf('.') + 1, y.length()));

								if (version < v2) // Compare the end numbers for the highest version
									version = v2;
							} catch (NumberFormatException e) {
							}
						}
					}

					studentDirectory.add(studentName + '.' + version);
				} catch (NumberFormatException e) {
				}
			}
		}

		return studentDirectory;
	}

	/**
	 * Initialize each student from directory names passed in and add the student's
	 * submissions
	 * 
	 * @param studentDirectories List of strings which correlate to student folder
	 *                           names
	 */
	public void getStudents(List<String> studentDirectories) {
		String assigndir = getAssignmentDirectory();

		for (String s : studentDirectories) {
			try {

				String studentName = (s.contains(".")) ? s.substring(0, s.indexOf('.')) : s;

				String dir = (assigndir.endsWith("/")) ? assigndir + s : assigndir + "/" + s;

				Student newstudent = new Student(studentName);

				newstudent.generateSubmissions(dir);

				addStudent(newstudent);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Executes the gathering of student directory names, creating student objects,
	 * and adding students to studentList.
	 * @throws IOException 
	 */
	public void instructorDriver() throws IOException {
		if (this.getIsError() || this.getIsHelp()) {
			return;
		}

		List<String> stringdirectories = this.getStudentDirectoryNames(this.getAssignmentDirectory());

		if (this.getIsError() || stringdirectories.isEmpty()) {
			this.setIsError(true);
			return;
		}
		getStudents(stringdirectories);
		for (Student s : studentList) {
			s.feedbackOutput();
		}
		scoreStudents();
		Spreadsheet mySheet = new Spreadsheet(scoredStudentOne, scoredStudentTwo, rawScores, normalizedScores);
		mySheet.setOutputPath(outputDirectory);
		if (templateName.length() > 0) {
			mySheet.setSheetName(templateName);
		}
		mySheet.generateSheet();
	}

	/**
	 * Scores the students in studentList based on similarities in their submission tokens.
	 * Generates two list of students as well as their raw and normalized scores.
	 */
	public void scoreStudents(){
		String masterToken = "";
		String sizeContainer = "";
		String sizeContainer2 = "";
		Student tempStudent = new Student();
		SharedPhrases phrases = new SharedPhrases();
		int studentCounter = 0;
		int studentCounter2 = 1;
		int L1 = 0;
		int L2 = 0;
		double total = 0;
		double ftotal = 0;
		int k = 0;
		String first = "";
		String second = "";
		
		///Get all tokens from all the submissions from all the students and add them to phrases.
		//Iterate through the student list.
		for(int i = 0; i < studentList.size(); i++) {
			tempStudent = studentList.get(i);
			//Iterate through the submissions of that student
			for(int s = 0; s < tempStudent.getSubmissions().size(); s++ ) {
				masterToken = "";
				//Iterate through the tokens and add them to a master list of tokens for this student.
				for(int t = 0; t < tempStudent.getSubmissions().get(s).getTokens().size(); t++) {
					masterToken = masterToken + tempStudent.getSubmissions().get(s).getTokens().get(t).getLexeme();
				}
				//Add the token list for this submission and identify it with the student's name.
				
				phrases.addSentence(masterToken, tempStudent.getName());
				masterToken = "";
			}
		}
		//Start with 1 student, iterate through the entire list of students, and compare the one student with all other students.
		while(studentCounter < studentList.size()) {
			if(studentCounter2 == studentList.size()) {
				break;
			}
			String temp;
			first = studentList.get(studentCounter).getName();
			second = studentList.get(studentCounter2).getName();
			//Iterate through all the shared phrases.
			for (CharSequence p: phrases.allPhrases()) {
				//Look at the sources for each phrase. If student 1 and student 2 share a phrase, calculate the total.
				 if((phrases.sourcesOf(String.valueOf(p)).contains(first))&&(phrases.sourcesOf(String.valueOf(p)).contains(second))) {
				    temp = String.valueOf(p);
				    k = 0;
					k = phrases.sourcesOf(temp).size();
					if(k < 2) {
						k = 2;
					}
					total = total + ((p.length())/((k-1)*(k-1)));	
				}
			}
			sizeContainer = "";
			//Get the total size of all the tokens from all the submissions from the first student.
			for(int s = 0; s < studentList.get(studentCounter).getSubmissions().size(); s++ ) {
				for(int t = 0; t < studentList.get(studentCounter).getSubmissions().get(s).getTokens().size(); t++) {
					sizeContainer = sizeContainer + studentList.get(studentCounter).getSubmissions().get(s).getTokens().get(t).getLexeme();
				}
			}
			
			L1 = 0;
			L1 = sizeContainer.length();
			sizeContainer2 = "";	
			
			//Get the total size of all the tokens from all the submissions from the second student.
			for(int s = 0; s < studentList.get(studentCounter2).getSubmissions().size(); s++ ) {
				for(int t = 0; t < studentList.get(studentCounter2).getSubmissions().get(s).getTokens().size(); t++) {
					sizeContainer2 = sizeContainer2 + studentList.get(studentCounter2).getSubmissions().get(s).getTokens().get(t).getLexeme();
				}
			}
			L2 = 0;
			L2 = sizeContainer2.length();
			//Calculate the final total.
			ftotal = 0;
			if (L1 < 1) {
				L1 = 1;
			}
			if (L2 < 1) {
				L2 = 1;
			}
			ftotal = ((4 * total)/((L1+L2)*(L1+L2)));
			total = 0.0;
			
			//Add the first student to the first list of students.
			scoredStudentOne.add(first);
			//Add the second student to the second list of students.
			scoredStudentTwo.add(second);
			//Add their corresponding score.
			rawScores.add(ftotal);
			ftotal = 0.0;
			studentCounter2++;
			//If we've exhausted our list of students, iterate to the next student so that student can be compared with the rest of the students.
			if(studentCounter2 == studentList.size()) {
				studentCounter++;
				studentCounter2 = studentCounter + 1;
				
			}
		}	
		double sum = 0.0;
		double std = 0.0;
		double norm = 0.0;
		int length = rawScores.size();
		for (double num : rawScores) {
			sum += num;
		}
		
		double mean = sum / length;
		
		for (double num: rawScores) {
			std += Math.pow(num - mean, 2);
		}
		
		for (int i = 0; i < rawScores.size(); i++) {
			norm = (rawScores.get(i) - mean) / std;
			normalizedScores.add(norm);
			if(norm > 1) {
				studentList.get(i).setSuspicious(true);
			}
		}
	}


	/**
	 * Add a student to Instructor's studentList.
	 * 
	 * @param s Student object to add to studentList
	 */
	public void addStudent(Student s) {
		if (!s.getSuspicious())
			studentList.add(s);
	}

	/**
	 * Sets studentList to the list passed in.
	 * 
	 * @param studentList List of student objects
	 */
	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}

	/**
	 * Returns the list of students contained in studentList.
	 * 
	 * @return studentList List of students to be compared.
	 */
	public List<Student> getStudentList() {
		return studentList;
	}

	/**
	 * Sets scoredStudentOne to the list passed in.
	 * 
	 * @param scoredStudentOne List of strings
	 */
	public void setScoredStudentOne(List<String> ScoredStudentList) {
		this.scoredStudentOne = ScoredStudentList;
	}

	/**
	 * Returns the list of students contained in scoredStudentOne.
	 * 
	 * @return scoredStudentOne List of scored students.
	 */
	public List<String> getScoredStudentOne() {
		return scoredStudentOne;
	}

	/**
	 * Sets scoredStudentTwo to the list passed in.
	 * 
	 * @param scoredStudentTwo List of strings
	 */
	public void setScoredStudentTwo(List<String> ScoredStudentList) {
		this.scoredStudentTwo = ScoredStudentList;
	}

	/**
	 * Returns the list of students contained in scoredStudentTwo.
	 * 
	 * @return scoredStudentTwo List of scored students.
	 */
	public List<String> getScoredStudentTwo() {
		return scoredStudentTwo;
	}

	/**
	 * Sets rawScores to the list passed in.
	 * 
	 * @param rawScores List of Doubles
	 */
	public void setRawScores(List<Double> scores) {
		this.rawScores = scores;
	}

	/**
	 * Returns the list of raw scores contained in rawScores.
	 * 
	 * @return rawScores List of student raw scores.
	 */
	public List<Double> getRawScores() {
		return rawScores;
	}

	/**
	 * Sets normalizedScores to the list passed in.
	 * 
	 * @param normalizedScores List of Doubles
	 */
	public void setNormalizedScores(List<Double> scores) {
		this.normalizedScores = scores;
	}

	/**
	 * Returns the list of normalized scores contained in normalizedScores.
	 * 
	 * @return normalizedScores List of student normalized scores.
	 */
	public List<Double> getNormalizedScores() {
		return normalizedScores;
	}

	/**
	 * Sets the directory searched for student submissions.
	 * 
	 * @param assignpath Directory in string form assigned to assignmentDirectory.
	 */
	public void setAssignmentDirectory(String assignpath) {
		assignmentDirectory = assignpath;
	}

	/**
	 * Returns the value of assignmentDirectory.
	 * 
	 * @return assignmentDirectory Returns location of the assignment directory
	 */
	public String getAssignmentDirectory() {
		return assignmentDirectory;
	}

	/**
	 * Sets the directory where the results spreadsheet is output to.
	 * 
	 * @param outpath Directory in string form assigned to outputDirectory.
	 */
	public void setOutputDirectory(String outpath) {
		outputDirectory = outpath;
	}

	/**
	 * Returns the value of outputDirectory.
	 * 
	 * @return outputDirectory Returns the path where the scored spreadsheet is
	 *         written to.
	 */
	public String getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * Sets the name of the user-specified spreadsheet template.
	 * 
	 * @param tempname Filename of the user-specified spreadsheet template.
	 */
	public void setTemplateName(String tempname) {
		templateName = tempname;
	}

	/**
	 * Returns the value of templateName.
	 * 
	 * @return templateName The file name of the user-specified template.
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * Sets sheet name to place report data scores.
	 * 
	 * @param reportname Name of the user-specified tab to set reportSheetName to.
	 */
	public void setReportSheetName(String reportname) {
		reportSheetName = reportname;
	}

	/**
	 * Returns value of reportSheetName.
	 * 
	 * @return reportSheetName Name of the user-specified tab to write scores to.
	 */
	public String getReportSheetName() {
		return reportSheetName;
	}

	/**
	 * Sets the value of isError.
	 * 
	 * @param a True/false value to assign isError.
	 */
	public void setIsError(Boolean a) {
		isError = a;
	}

	/**
	 * Returns boolean isError.
	 * 
	 * @return isError Boolean determining if Instructor was improperly initialized.
	 */
	public Boolean getIsError() {
		return isError;
	}

	/**
	 * Sets isHelp, which signals if user ran CodeComp with -help parameter.
	 * 
	 * @param b True/false value to assign isHelp.
	 */
	public void setIsHelp(Boolean b) {
		isHelp = b;
	}

	/**
	 * Returns boolean isHelp
	 * 
	 * @return isHelp Boolean determining if user specified -help argument.
	 */
	public Boolean getIsHelp() {
		return isHelp;
	}

	/**
	 * Sets customTemplate, which signals if the user specified a custom spreadsheet
	 * template.
	 * 
	 * @param c True/false value to assign customTemplate.
	 */
	public void setCustomTemplate(Boolean c) {
		customTemplate = c;
	}

	/**
	 * Returns boolean customTemplate.
	 * 
	 * @return customTemplate Boolean determining if user specified a custom
	 *         template name.
	 */
	public Boolean getCustomTemplate() {
		return customTemplate;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Instructor instructor2 = new Instructor();
		instructor2.assignmentDirectory = this.assignmentDirectory;
		instructor2.outputDirectory = this.outputDirectory;
		instructor2.templateName = this.templateName;
		instructor2.reportSheetName = this.reportSheetName;
		instructor2.isError = this.isError;
		instructor2.isHelp = this.isHelp;
		instructor2.customTemplate = this.customTemplate;
		instructor2.scoredStudentOne = this.scoredStudentOne;
		instructor2.rawScores = this.rawScores;
		instructor2.normalizedScores = this.normalizedScores;
		List<Student> studentList2 = new ArrayList<Student>();
		for (Student s : studentList) {
			studentList2.add((Student) s.clone());
		}
		instructor2.studentList = studentList2;
		return instructor2;
	}

	/**
	 * Returns object hash code.
	 * 
	 * @return result Integer representing Instructor's hash code.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime * (assignmentDirectory.hashCode() + customTemplate.hashCode() + isError.hashCode()
				+ isHelp.hashCode() + normalizedScores.size() + outputDirectory.hashCode() + rawScores.size()
				+ reportSheetName.hashCode() + scoredStudentOne.size() + scoredStudentTwo.size()
				+ studentList.size() + templateName.hashCode());
		return result;
	}

	/**
	 * Returns object hash code.
	 * 
	 * @param obj Instructor object to compare this to.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		Instructor other = (Instructor) obj;
		if (assignmentDirectory == other.assignmentDirectory && outputDirectory == other.outputDirectory
				&& this.assignmentDirectory == other.assignmentDirectory
				&& this.outputDirectory == other.outputDirectory && this.templateName == other.templateName
				&& this.reportSheetName == other.reportSheetName && this.isError == other.isError
				&& this.isHelp == other.isHelp && this.customTemplate == other.customTemplate
				&& this.scoredStudentOne == other.scoredStudentOne && this.rawScores == other.rawScores
				&& this.normalizedScores == other.normalizedScores) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Directory containing student submissions.
	 */
	private String assignmentDirectory = "";
	/**
	 * Directory to output report spreadsheet.
	 */
	private String outputDirectory = "";
	/**
	 * Name of user-specified spreadsheet template.
	 */
	private String templateName = "";
	/**
	 * Name of sheet name where report data is written to
	 */
	private String reportSheetName = DEFAULT_SHEET_NAME;
	/**
	 * Boolean flag signaling incorrect parameters.
	 */
	private Boolean isError = false;
	/**
	 * Boolean flag signaling -help parameter used.
	 */
	private Boolean isHelp = false;
	/**
	 * Boolean flag signaling custom spreadsheet template specification.
	 */
	private Boolean customTemplate = false;

	/**
	 * List of Students to be scored.
	 */
	private List<Student> studentList = new ArrayList<Student>();
	/**
	 * List of scored Students meant to populate spreadsheet column one.
	 */
	private List<String> scoredStudentOne = new ArrayList<String>();
	/**
	 * List of scored Students meant to populate spreadsheet column two.
	 */
	private List<String> scoredStudentTwo = new ArrayList<String>();
	/**
	 * List of raw scores meant to populate spreadsheet column three.
	 */
	private List<Double> rawScores = new ArrayList<Double>();
	/**
	 * List of normalized scores meant to population spreadsheet column four.
	 */
	private List<Double> normalizedScores = new ArrayList<Double>();

}
