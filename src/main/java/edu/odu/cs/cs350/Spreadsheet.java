package edu.odu.cs.cs350;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Spreadsheet Class This class processes a list of students, another list of
 * students, a list of raw scores, and a list of z scores and outputs this
 * information in to a spreadsheet. The spreadsheet name can be changed.
 */
public class Spreadsheet implements Cloneable {
	/**
	 * String member that contains the name of the spreadsheet. The default name is "Report".
	 */
	private String templateName = "Report";
	/**
	 * String member that contains the name of the sheet. The default name is "sheet1".
	 */
	private String sheetName = "sheet1";
	/**
	 * String member that contains the output path of the spreadsheet xls file. If not specified, it will be stored in the directory the program is run in.
	 */
	private String outputPath = "./";
	/**
	 * List of student strings that will be stored in column one of the spreadsheet.
	 */						
	private List<String> student1; 
	/**
	 * List of student strings that will be stored in column two of the spreadsheet.
	 */
	private List<String> student2; 
	/**
	 * List of Doubles that contain the raw scores of the compared students. This will occupy column three of the spreadsheet.
	 */
	private List<Double> rawScore; 
	/**
	 * List of Doubles that contain the normalized scores of the compared students. This will occupy column four of the spreadsheet.
	 */
	private List<Double> zScore; 

	/**
	 * Default constructor for Spreadsheet class. Sets spreadsheet's name to the
	 * default "Report" and initializes all lists(these are empty).
	 */

	public Spreadsheet() {
		templateName = "Report";
		sheetName = "sheet1";
		outputPath = "";
		student1 = new ArrayList<String>();
		student2 = new ArrayList<String>();
		rawScore = new ArrayList<Double>();
		zScore = new ArrayList<Double>();
	}

	/**
	 * Overloaded constructor for Spreadsheet class. Sets the members of
	 * Spreadsheet(except for sheetName) according to the parameters given to the
	 * constructor.
	 * 
	 * @param s1 - List of student names.
	 * @param s2 - Second list of student names.
	 * @param rs - List of raw scores.
	 * @param zs - List of z scores.
	 */
	public Spreadsheet(List<String> s1, List<String> s2, List<Double> rs, List<Double> zs) {
		this.student1 = s1;
		this.student2 = s2;
		this.rawScore = rs;
		this.zScore = zs;
	}
	/**
	 * Gets the spreadsheet's name.
	 * 
	 * @return the name of the spreadsheet.
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * Sets the spreadsheet's name.
	 * 
	 * @param the name of the spreadsheet to be set.
	 */
	public void setTemplateName(String n) {
		templateName = n;
	}
	/**
	 * Gets the spreadsheet's name.
	 * 
	 * @return the name of the spreadsheet.
	 */
	public String getSheetName() {
		return sheetName;
	}

	/**
	 * Sets the spreadsheet's name.
	 * 
	 * @param the name of the spreadsheet to be set.
	 */
	public void setSheetName(String n) {
		sheetName = n;
	}

	/**
	 * Gets the spreadsheet's name.
	 * 
	 * @return the name of the spreadsheet.
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * Sets the spreadsheet's name.
	 * 
	 * @param the name of the spreadsheet to be set.
	 */
	public void setOutputPath(String n) {
		outputPath = n;
	}

	/**
	 * Gets the spreadsheet's first list of students.
	 * 
	 * @return spreadsheet's first list of students.
	 */
	public List<String> getStudent1() {
		return student1;
	}

	/**
	 * Sets the spreadsheet's first list of students.
	 * 
	 * @param the list of students to be set.
	 */
	public void setStudent1(List<String> s) {
		student1 = s;
	}

	/**
	 * Gets the spreadsheet's second list of students.
	 * 
	 * @return spreadsheet's second list of students.
	 */
	public List<String> getStudent2() {
		return student2;
	}

	/**
	 * Sets the spreadsheet's second list of students.
	 * 
	 * @param the list of students to be set.
	 */
	public void setStudent2(List<String> s) {
		student2 = s;
	}

	/**
	 * Gets the spreadsheet's list of raw scores.
	 * 
	 * @return spreadsheet's list of raw scores.
	 */
	public List<Double> getRawScore() {
		return rawScore;
	}

	/**
	 * Sets the spreadsheet's list of raw scores.
	 * 
	 * @param the list of raw scores to be set.
	 */
	public void setRawScore(List<Double> s) {
		rawScore = s;
	}

	/**
	 * Gets the spreadsheet's list of z scores.
	 * 
	 * @return spreadsheet's list of z scores.
	 */
	public List<Double> getZScore() {
		return zScore;
	}

	/**
	 * Sets the spreadsheet's list of z scores.
	 * 
	 * @param the list of z scores to be set.
	 */
	public void setZScore(List<Double> s) {
		zScore = s;
	}

	/**
	 * Adds strings(student names) to the spreadsheet's first list of students.
	 * Checks for an existing student name before adding it to the list.
	 * 
	 * @param String as - a student name.
	 */
	public void addStudent1s(String as) {

		boolean found = false;
		for (String existingStudent1 : student1) {
			if (existingStudent1.equals(as)) {
				found = true;
				break;
			}
		}
		if (found == false) {
			student1.add(as);
		}

	}

	/**
	 * Adds strings(student names) to the spreadsheet's second list of students.
	 * Checks for an existing student name before adding it to the list.
	 * 
	 * @param String as - a student name.
	 */
	public void addStudent2s(String as) {

		boolean found = false;
		for (String existingStudent2 : student2) {
			if (existingStudent2.equals(as)) {
				found = true;
				break;
			}
		}
		if (found == false) {
			student2.add(as);
		}

	}

	/**
	 * Adds Doubles(raw scores) to the spreadsheet's list of raw scores. Does not
	 * check for duplicate scores as scores MAY be the same.
	 * 
	 * @param Double as - a z score.
	 */
	public void addRawScores(Double as) {
		rawScore.add(as);
	}

	/**
	 * Adds Doubles(z scores) to the spreadsheet's list of z scores. Does not check
	 * for duplicate scores as scores MAY be the same.
	 * 
	 * @param Double as - a z score.
	 */
	public void addZScores(Double as) {
		zScore.add(as);
	}

	/**
	 * Outputs the classes lists to a spreadsheet named "Report" by default.
	 * Spreadsheet name can be changed via setting sheetName.
	 */
	public void generateSheet() throws IOException {
		// Creating a workbook with a new sheet.
		Workbook wb = new HSSFWorkbook();
		Sheet s = wb.createSheet("sheetName");
		// Creating the first row for the header text.
		Row headerRow = s.createRow(0);
		// Creating the style for the spreadsheet.
		Font font = wb.createFont();
		font.setBold(true); // Establishing bold font.
		CellStyle header = wb.createCellStyle(); // Creating a style to apply to the header cells.
		header.setAlignment(HorizontalAlignment.CENTER); // Centering
		header.setFont(font); // Applying bold font to header style.
		CellStyle body = wb.createCellStyle(); // Creating a style to apply to the rest of the sheet.
		body.setAlignment(HorizontalAlignment.CENTER);
		CellStyle zScoreCell = wb.createCellStyle(); // Creating a style to right-align the z-score column
		zScoreCell.setAlignment(HorizontalAlignment.RIGHT);
		// Creating four header cells.
		Cell cell1 = headerRow.createCell(0);
		Cell cell2 = headerRow.createCell(1);
		Cell cell3 = headerRow.createCell(2);
		Cell cell4 = headerRow.createCell(3);
		// Setting the style we created to the header cells.
		cell1.setCellStyle(header);
		cell2.setCellStyle(header);
		cell3.setCellStyle(header);
		cell4.setCellStyle(header);
		// Filling the header cells with values.
		cell1.setCellValue("Student 1");
		cell2.setCellValue("Student 2");
		cell3.setCellValue("Raw Score");
		cell4.setCellValue("z Score");

		// The point of the spreadsheet is to take the lists in the Spreadsheet class
		// and output them to a workbook file (Default : Report).
		// We take the size of the student list(all lists should be the same size as
		// comparisons should not have happened with missing data), and loop
		// as many times as the size of the list. This fills the columns with student1
		// names, student2 names, and their corresponding scores.
		for (int i = 0; i < student1.size(); i++) {
			Row newRow = s.createRow(i + 1); // Creating a new row. Row 0 is used for headers, so we must add 1 to i so
												// we start with row 1 instead of 0.
			// Creating four cells for the new row.
			cell1 = newRow.createCell(0);
			cell2 = newRow.createCell(1);
			cell3 = newRow.createCell(2);
			cell4 = newRow.createCell(3);
			// Applying the cell style we created above to the cells.
			cell1.setCellStyle(body);
			cell2.setCellStyle(body);
			cell3.setCellStyle(body);
			// The z score cell(cell 4) gets its own special right-aligned style.
			cell4.setCellStyle(zScoreCell);
			// Putting the information from our lists into the cells.
			cell1.setCellValue(student1.get(i));
			cell2.setCellValue(student2.get(i));
			cell3.setCellValue(rawScore.get(i));
			cell4.setCellValue(zScore.get(i));

		}
		// Making sure our sheet has the proper file extension.
		String newTemplateName = outputPath + templateName + ".xls";
		// Creating the actual spreadsheet file.
		try (OutputStream fileOut = new FileOutputStream(newTemplateName)) {
			wb.write(fileOut); // Write the file
		}
		wb.close(); // Close the workbook.

	}

	/**
	 * Puts the spreadsheet name into string buffer.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Spreadsheet Name: ");
		buf.append(templateName);
		return buf.toString();
	}

	/**
	 * Determines equivalency of two spreadsheets.
	 * 
	 * @param Object obj - a spreadsheet object.
	 * @return True if the same, false if not.
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof Spreadsheet)) {
			return false;
		}
		Spreadsheet other = (Spreadsheet) obj;
		if ((sheetName.contentEquals(other.sheetName)) && (zScore.equals(other.zScore))
				&& (student1.equals(other.student1)) && (student2.equals(other.student2))
				&& (rawScore.equals(other.rawScore) && (templateName.contentEquals(other.templateName)))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Creates a hash code.
	 * 
	 * @return a hash code.
	 */
	public int hashCode() {
		int combinedHash = 0;
		combinedHash = rawScore.hashCode() + zScore.hashCode();
		return combinedHash;
	}

	/**
	 * Deep copies a spreadsheet object.
	 * 
	 * @return a copy of a spreadsheet object.
	 */
	public Object clone() {
		Spreadsheet sheetClone = new Spreadsheet(student1, student2, rawScore, zScore);
		sheetClone.templateName = templateName;
		sheetClone.sheetName = sheetName;
		return sheetClone;
	}

	/**
	 * Provides an iterator for the student1 list.
	 * 
	 * @return an iterator for the student1 list.
	 */
	public Iterator<String> S1Iterator() {
		return student1.iterator();
	}

	/**
	 * Provides an iterator for the student2 list.
	 * 
	 * @return an iterator for the student1 list.
	 */
	public Iterator<String> S2Iterator() {
		return student2.iterator();
	}

	/**
	 * Provides an iterator for the raw scores list.
	 * 
	 * @return an iterator for the raw scores list.
	 */
	public Iterator<Double> RSIterator() {
		return rawScore.iterator();
	}

	/**
	 * Provides an iterator for the z scores list.
	 * 
	 * @return an iterator for the z scores list.
	 */
	public Iterator<Double> ZSIterator() {
		return rawScore.iterator();
	}
}