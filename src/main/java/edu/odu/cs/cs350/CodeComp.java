package edu.odu.cs.cs350;

import java.io.IOException;

/**
 * Main program class.
 */
public class CodeComp {
	/**
	 * Main program driver.
	 * 
	 * @param args parameters specified by user.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		// create Instructor class with arguments
		Instructor newIns = new Instructor(args);
		newIns.instructorDriver();

		if (newIns.getIsError()) {
			System.out.println("Insufficient Parameters. For Help: java -jar CodeComp.jar -help");
		}

		if (!newIns.getIsError() && newIns.getIsHelp()) {
			printHelp();
		}

		if (!newIns.getIsError() && !newIns.getIsHelp()) {
			newIns.instructorDriver();
		}
	}

	/**
	 * Prints CodeComp usage instructions to output stream.
	 */
	public static void printHelp() {
		System.out.println("Instructions: java -jar CodeComp.jar [options] [assignmentDirectory] [outputDirectory]");
		System.out.println("Available options:");
		System.out.println("-template [templateSpreadsheet]");
		System.out.println("Replaces the default spreadsheet template with one specified by the user.");
		System.out.println("-report [sheetname]");
		System.out.println(
				"Replaces the sheet name used to identify where to place the report data scores (default is \"Report\").");
		System.out.println("-help");
		System.out.println("Prints a summary of the command line parameters.");

	}
}