/*
 * Adapted from java.flex packaged with the jflex source
 * Original Copyright (C) 1998-2018  Gerwin Klein <lsf@jflex.de>
 */

package edu.odu.cs.cs350;

%%

%public
%class CppScanner
%type Token
%line
%column
%unicode

%{
	StringBuffer string = new StringBuffer();

    private Token symb(TokenEnum type) {
    	return new Token(type, yyline+1, yycolumn+1, false);
    }
    private Token symb(TokenEnum type, String value) {
    	return new Token(type, yyline+1, yycolumn+1, value, false);
    }
%}

/* main character classes */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]

WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | 
          {DocumentationComment}

TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?
DocumentationComment = "/*" "*"+ [^/*] ~"*/"

/* identifiers */
Identifier = [:jletter:][:jletterdigit:]*

/* integer literals */
DecIntegerLiteral = 0 | [1-9][0-9]*
DecLongLiteral    = {DecIntegerLiteral} [lL]

HexIntegerLiteral = 0 [xX] 0* {HexDigit} {1,8}
HexLongLiteral    = 0 [xX] 0* {HexDigit} {1,16} [lL]
HexDigit          = [0-9a-fA-F]

OctIntegerLiteral = 0+ [1-3]? {OctDigit} {1,15}
OctLongLiteral    = 0+ 1? {OctDigit} {1,21} [lL]
OctDigit          = [0-7]
    
/* floating point literals */        
FloatLiteral  = ({FLit1}|{FLit2}|{FLit3}) {Exponent}? [fF]
DoubleLiteral = ({FLit1}|{FLit2}|{FLit3}) {Exponent}?

FLit1    = [0-9]+ \. [0-9]* 
FLit2    = \. [0-9]+ 
FLit3    = [0-9]+ 
Exponent = [eE] [+-]? [0-9]+

/* string and character literals */
StringCharacter = [^\r\n\"\\]
SingleCharacter = [^\r\n\'\\]

%state STRING, CHARLITERAL

%%

<YYINITIAL> {
	"auto" {return symb(TokenEnum.AUTO, yytext());}
	"bool" {return symb(TokenEnum.BOOLEAN, yytext());}
	"break" {return symb(TokenEnum.BREAK, yytext());}
	"case" {return symb(TokenEnum.CASE, yytext());}
	"catch" {return symb(TokenEnum.CATCH, yytext());}
	"char" {return symb(TokenEnum.CHAR, yytext());}
	"class" {return symb(TokenEnum.CLASS, yytext());}
	"const" {return symb(TokenEnum.CONST, yytext());}
	"continue" {return symb(TokenEnum.CONTINUE, yytext());}
	"do" {return symb(TokenEnum.DO, yytext());}
	"double" {return symb(TokenEnum.DOUBLE, yytext());}
	"else" {return symb(TokenEnum.ELSE, yytext());}
	"enum" {return symb(TokenEnum.ENUM, yytext());}
	"final" {return symb(TokenEnum.FINAL, yytext());}
	"float" {return symb(TokenEnum.FLOAT, yytext());}
	"for" {return symb(TokenEnum.FOR, yytext());}
	"default" {return symb(TokenEnum.DEFAULT, yytext());}
	"import" {return symb(TokenEnum.IMPORT, yytext());}
	"int" {return symb(TokenEnum.INT, yytext());}
	"long" {return symb(TokenEnum.LONG, yytext());}
	"new" {return symb(TokenEnum.NEW, yytext());}
	"goto" {return symb(TokenEnum.GOTO, yytext());}
	"if" {return symb(TokenEnum.IF, yytext());}
	"public" {return symb(TokenEnum.PUBLIC, yytext());}
	"short" {return symb(TokenEnum.SHORT, yytext());}
	"switch" {return symb(TokenEnum.SWITCH, yytext());}
	"string" {return symb(TokenEnum.STRING, yytext());}	
	"private" {return symb(TokenEnum.PRIVATE, yytext());}
	"protected" {return symb(TokenEnum.PROTECTED, yytext());}
	"return" {return symb(TokenEnum.RETURN, yytext());}
	"void" {return symb(TokenEnum.VOID, yytext());}
	"static" {return symb(TokenEnum.STATIC, yytext());}
	"std" {return symb(TokenEnum.STD, yytext());}
	"while" {return symb(TokenEnum.WHILE, yytext());}
	"this" {return symb(TokenEnum.THIS, yytext());}
	"throw" {return symb(TokenEnum.THROW, yytext());}
	"try" {return symb(TokenEnum.TRY, yytext());}
	
	"true" {return symb(TokenEnum.BOOLEAN_LITERAL, "true");}
	"false" {return symb(TokenEnum.BOOLEAN_LITERAL, "false");}
	"null" {return symb(TokenEnum.NULL_LITERAL, yytext());}
	  
	"=" {return symb(TokenEnum.EQ, yytext());}
	">" {return symb(TokenEnum.GT, yytext());}
	"<" {return symb(TokenEnum.LT, yytext());}
	"!" {return symb(TokenEnum.NOT, yytext());}
	"~" {return symb(TokenEnum.COMP, yytext());}
	"?" {return symb(TokenEnum.QUESTION, yytext());}
	"::" {return symb(TokenEnum.COLONCOLON, yytext());}
	":" {return symb(TokenEnum.COLON, yytext());}	
	"==" {return symb(TokenEnum.EQEQ, yytext());}
	"<=" {return symb(TokenEnum.LTEQ, yytext());}
	">=" {return symb(TokenEnum.GTEQ, yytext());}
	"!=" {return symb(TokenEnum.NOTEQ, yytext());}
	"&&" {return symb(TokenEnum.ANDAND, yytext());}
	"||" {return symb(TokenEnum.OROR, yytext());}
	"++" {return symb(TokenEnum.PLUSPLUS, yytext());}
	"--" {return symb(TokenEnum.MINUSMINUS, yytext());}
	"+" {return symb(TokenEnum.PLUS, yytext());}
	"-" {return symb(TokenEnum.MINUS, yytext());}
	"*" {return symb(TokenEnum.MULT, yytext());}
	"/" {return symb(TokenEnum.DIV, yytext());}
	"&" {return symb(TokenEnum.AND, yytext());}
	"|" {return symb(TokenEnum.OR, yytext());}
	"^" {return symb(TokenEnum.XOR, yytext());}
	"%" {return symb(TokenEnum.MOD, yytext());}
	"<<" {return symb(TokenEnum.LSHIFT, yytext());}
	">>" {return symb(TokenEnum.RSHIFT, yytext());}
	">>>" {return symb(TokenEnum.URSHIFT, yytext());}
	"+=" {return symb(TokenEnum.PLUSEQ, yytext());}
	"-=" {return symb(TokenEnum.MINUSEQ, yytext());}
	"*=" {return symb(TokenEnum.MULTEQ, yytext());}
	"/=" {return symb(TokenEnum.DIVEQ, yytext());}
	"&=" {return symb(TokenEnum.ANDEQ, yytext());}
	"|=" {return symb(TokenEnum.OREQ, yytext());}
	"^=" {return symb(TokenEnum.XOREQ, yytext());}
	"%=" {return symb(TokenEnum.MODEQ, yytext());}
	"<<=" {return symb(TokenEnum.LSHIFTEQ, yytext());}
	">>=" {return symb(TokenEnum.RSHIFTEQ, yytext());}
	">>>=" {return symb(TokenEnum.URSHIFTEQ, yytext());}
	\" {string.setLength(0); yybegin(STRING);}
	\' {yybegin(CHARLITERAL);}
	
  /* numeric literals */

  /* This is matched together with the minus, because the number is too big to 
     be represented by a positive integer. */
    "-2147483648" { return symb(TokenEnum.INTEGER_LITERAL, Integer.toString(Integer.MIN_VALUE)); }
  {DecIntegerLiteral}            { return symb(TokenEnum.INTEGER_LITERAL, yytext()); }
  {DecLongLiteral}               { return symb(TokenEnum.INTEGER_LITERAL, yytext().substring(0,yylength()-1)); }
  
  {HexIntegerLiteral}            { return symb(TokenEnum.INTEGER_LITERAL, yytext()); }
  {HexLongLiteral}               { return symb(TokenEnum.INTEGER_LITERAL, yytext().substring(0,yylength()-1)); }
 
  {OctIntegerLiteral}            { return symb(TokenEnum.INTEGER_LITERAL, yytext()); }  
  {OctLongLiteral}               { return symb(TokenEnum.INTEGER_LITERAL, yytext().substring(0,yylength()-1)); }
  
  {FloatLiteral}                 { return symb(TokenEnum.FLOATING_POINT_LITERAL, yytext().substring(0,yylength()-1)); }
  {DoubleLiteral}                { return symb(TokenEnum.FLOATING_POINT_LITERAL, yytext().substring(0,yylength()-1)); }
  {DoubleLiteral}[dD]            { return symb(TokenEnum.FLOATING_POINT_LITERAL, yytext().substring(0,yylength()-1)); }
  
  /* comments */
  {Comment}                      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }

  /* identifiers */ 
  {Identifier}                   { return symb(TokenEnum.IDENTIFIER, yytext()); }  
}

<STRING> {
  \"                             { yybegin(YYINITIAL); return symb(TokenEnum.STRING_LITERAL, string.toString()); }
  
  {StringCharacter}+             { string.append( yytext() ); }
  
  /* escape sequences */
  "\\b"                          { string.append( '\b' ); }
  "\\t"                          { string.append( '\t' ); }
  "\\n"                          { string.append( '\n' ); }
  "\\f"                          { string.append( '\f' ); }
  "\\r"                          { string.append( '\r' ); }
  "\\\""                         { string.append( '\"' ); }
  "\\'"                          { string.append( '\'' ); }
  "\\\\"                         { string.append( '\\' ); }
  \\[0-3]?{OctDigit}?{OctDigit}  { char val = (char) Integer.parseInt(yytext().substring(1),8);
                        				   string.append( val ); }
  
  /* error cases */
  \\.                            {}
  {LineTerminator}               {}
}

<CHARLITERAL> {
  {SingleCharacter}\'            { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, yytext().substring(0,1)); }
  
  /* escape sequences */
  "\\b"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\b");}
  "\\t"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\t");}
  "\\n"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\n");}
  "\\f"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\f");}
  "\\r"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\r");}
  "\\\""\'                       { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\"");}
  "\\'"\'                        { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\'");}
  "\\\\"\'                       { yybegin(YYINITIAL); return symb(TokenEnum.CHARACTER_LITERAL, "\\"); }
  \\[0-3]?{OctDigit}?{OctDigit}\' { yybegin(YYINITIAL); 
			                              int val = Integer.parseInt(yytext().substring(1,yylength()-1),8);
			                            return symb(TokenEnum.CHARACTER_LITERAL, Integer.toString(val)); }
  
  /* error cases */
  \\.                            {}
  {LineTerminator}               {}
}

. {}
[^] {}
<<EOF>> {return symb(TokenEnum.EOF);}